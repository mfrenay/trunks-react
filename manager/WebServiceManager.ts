import ApiResponse, {API_RESPONSE_STATUS} from "trunks-core-js/web-service/dto/ApiResponse";
import ErrorManager from "../ui/ErrorManager";

const WebServiceManager = {

    /**
     * manageMessage
     * @param callback
     */
    manageMessage(callback: (_: ApiResponse) => void) {
        return async (jsonResponse: ApiResponse) => {
            if (jsonResponse.status === API_RESPONSE_STATUS.warning) {
                ErrorManager.displayWarningMessage(jsonResponse.message);
            }
            await callback(jsonResponse);
        }
    }
}

export default WebServiceManager;
