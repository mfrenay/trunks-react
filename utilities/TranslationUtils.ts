import {TFunction} from "i18next";
import StringUtils from "trunks-core-js/utilities/StringUtils";

const TranslationUtils = {

    /**
     * getErrorTranslation
     * @param t
     * @param errorCode
     */
    getErrorTranslation(t: TFunction, errorCode: string): string {
        errorCode = StringUtils.getNString(errorCode);
        const params = [];
        if (errorCode !== "") {
            const parts = errorCode.split(";");
            errorCode = parts[0];
            if (parts.length > 1) {
                for (let k = 1; k < parts.length; k++) {
                    params.push(parts[k]);
                }
            }
        }

        errorCode = errorCode.toLowerCase();
        let translationKey = errorCode;
        let translatedMsg = t(translationKey);

        if (translatedMsg === translationKey) {
            // Translation not found
            translationKey = "default.msg_error." + errorCode;
            translatedMsg = t(translationKey);

            if (translatedMsg === translationKey) {
                const keySeparator = ".";
                // Translation not found
                const splittedErrorCode = errorCode.split(keySeparator);

                if (splittedErrorCode.length === 1 || (splittedErrorCode.length === 2 && splittedErrorCode[1] === "default")) {
                    return t("default.msg_error.general");
                } else {
                    return this.getErrorTranslation(t, splittedErrorCode[0] + ".default");
                }
            }
        }

        // replace parameters #x#
        translatedMsg = StringUtils.replaceParametersInExpression(translatedMsg, params);

        return translatedMsg;
    },
}

export default TranslationUtils;
