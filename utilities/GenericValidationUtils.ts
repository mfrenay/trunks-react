import TForm from "trunks-core-js/model/TForm";
import TField from "trunks-core-js/model/TField";
import {config} from "../config";
import TFieldKind from "trunks-core-js/model/TFieldKind";
import TRow from "trunks-core-js/model/TRow";
import TList from "trunks-core-js/model/TList";
import StringUtils from "trunks-core-js/utilities/StringUtils";

export interface IListError {
    nrRow: number,
    errors: string[]
}

export interface IRowError {
    [id: string]: string[];
}

export interface IFormError {
    generalErrors?: string[];
    fieldErrors?: {
        [id: string]: string[];
    }
}

const GenericValidationUtils = {
    /**
     * Generic validation of all row's fields of a TList
     * @param tList
     * @param options
     */
    validateList(tList: TList, options: Partial<{maxNbRowCheck: number}> = {}): IListError[] {
        const ret: IListError[] = [];

        for (let i = 0, baseRow = tList.baseRow; i < tList.rows.length; i++) {
            const row = tList.rows[i];
            const errors: IRowError = this.validateRow(row, baseRow);

            if (Object.values(errors).length > 0) {
                ret.push({
                    nrRow: i + 1,
                    errors: Object.values(errors).flat()
                });
            }

            if (options.maxNbRowCheck && ret.length >= options.maxNbRowCheck) {
                return ret;
            }
        }

        return ret;
    },

    /**
     * Generic validation of all fields of a TForm
     * @param tForm
     */
    validateForm(tForm: TForm): IFormError {
        // tForm.baseRow.fields.forEach((baseField) => {
        //     if (baseField instanceof TField) {
        //         errors.push(...this.validateField(tForm.row.getFieldById(baseField.id)!, baseField));
        //     }
        // });

        return {
            generalErrors: [],
            fieldErrors: this.validateRow(tForm.row, tForm.baseRow)
        };
    },

    /**
     * Generic validation of all fields of a TRow (based on their baseFields)
     * @param row
     * @param baseRow
     */
    validateRow(row: TRow, baseRow: TRow, isAddFooterRow: boolean = false): IRowError {
        const errors: IRowError = {};

        baseRow.fields.forEach(baseField => {
            const field = row.getFieldById(baseField.id);

            const err = this.validateField(field!, baseField as TField, isAddFooterRow);

            if (err.length > 0) {
                errors[baseField.id] = [...(errors[baseField.id] || []), ...err];
            }
        });

        return errors;
    },

    /**
     * Generic validation of a field (based on its baseField)
     * @param field
     * @param baseField
     */
    validateField(field: TField, baseField: TField, isAddFooterRow: boolean = false): string[] {
        const {t} = config;
        const errors: string[] = [];

        const fieldValue = field.stringValue;

        // validation mandatory
        if ((!isAddFooterRow && baseField.isVisible) || (isAddFooterRow && baseField.isVisibleAdd)) {
            // validation mandatory
            if (baseField.isMandatory && baseField.kind !== TFieldKind.CHECKBOX) {

                if (fieldValue === "") {
                    errors.push(t("default.form_error.field_mandatory", {fieldLabel: t(baseField.label!)}));
                    return errors;
                }
            }

            // validation MIN_VAL and MAX_VAL
            if (fieldValue !== "" && (baseField.type === "INTEGER" || baseField.type === "NUMBER")) {
                const minVal = StringUtils.getNString(baseField.customObjectParams?.MIN);
                const maxVal = StringUtils.getNString(baseField.customObjectParams?.MAX);

                if (minVal !== "" && Number(fieldValue) < Number(minVal)) {
                    if (maxVal === "") {
                        errors.push(t("default.form_error.field_min_value", {fieldLabel: t(baseField.label!), min: minVal}));
                    }
                    else {
                        errors.push(t("default.form_error.field_between_value", {fieldLabel: t(baseField.label!), min: minVal, max: maxVal}));
                    }
                }
                else
                if (maxVal !== "" && Number(fieldValue) > Number(maxVal)) {
                    if (minVal === "") {
                        errors.push(t("default.form_error.field_max_value", {fieldLabel: t(baseField.label!), max: maxVal}));
                    }
                    else {
                        errors.push(t("default.form_error.field_between_value", {fieldLabel: t(baseField.label!), min: minVal, max: maxVal}));
                    }
                }
            }

            // TODO: validation regex
            // TODO: validation generic FORMAT (email)
        }

        return errors;
    }
}

export default GenericValidationUtils;
