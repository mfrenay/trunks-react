import {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  FC,
  MouseEvent,
  MouseEventHandler,
  PropsWithChildren,
  useEffect,
  useRef,
  useState
} from "react";

interface AsyncButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  // Force onClick props to be a Promise-returned function
  onClick?: (mouseEvent: MouseEvent<HTMLButtonElement>) => Promise<any>;
}

/**
 * Renders a button with an async onClick function that is disabled until the async response is received.
 *
 * The onClick props must return a Promise.
 * The button becomes disabled once the onClick event is launched, and is re-enable only after the Promise returned by the onClick is finished.
 *
 * @param props
 * @constructor
 */
const AsyncButton: FC<PropsWithChildren<AsyncButtonProps>> = (props) => {
  const [isDisabled, setIsDisabled] = useState(false);
  const mounted = useRef(true); // Used to prevent setting state if the component is unmounted

  useEffect(() => {
    return () => {
      mounted.current = false;
    }
  }, []);

  const handleOnClick: MouseEventHandler<HTMLButtonElement> = async (e) => {
    setIsDisabled(true);

    try {
      await props.onClick?.(e);
    } finally {
      // Set state only if component is still mounted
      // Maybe it has been unmounted by parent on props.onClick
      if (mounted.current) {
        setIsDisabled(false);
      }
    }
  }

  return (
    <button
      {...props}
      disabled={props.disabled || isDisabled}
      onClick={handleOnClick}
    >
      {props.children}
    </button>
  );
}


export default AsyncButton;
