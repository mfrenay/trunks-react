import React, {Fragment} from "react";
import {IListError} from "../../utilities/GenericValidationUtils";
import {config} from "../../config";

export interface IListErrorsProps {
    listErrors: IListError[] | null;
}

function ListErrors({listErrors}: IListErrorsProps) {
    if (!listErrors || listErrors.length === 0) {
        return null;
    }

    const {t} = config;

    return (
        <div className="alert alert-danger list-errors" role="alert">
            {listErrors.map((listError) => (
                <Fragment key={listError.nrRow + listError.errors.join("-")}>
                    <h3>{t("list_errors.row_line", {nrRow: listError.nrRow})}</h3>
                    <ul key={listError.nrRow}>
                        {listError.errors.map((error) =>
                            <li key={error}>{error}</li>
                        )}
                    </ul>
                </Fragment>
            ))}
        </div>
    );
}

export default ListErrors;
