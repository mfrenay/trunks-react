interface IProps {
    formErrors?: string[];
}

function FormErrors({formErrors}: IProps) {
    if (formErrors == null || formErrors.length === 0) {
        return null;
    }

    return (
        <div className="alert alert-danger form-errors" role="alert">
          {formErrors.length === 1 ? (
            <>
              {formErrors[0]}
            </>
          ) : (
            <ul>
              {formErrors.map((error) =>
                <li key={error}>{error}</li>
              )}
            </ul>
          )}
        </div>
    );
}

export default FormErrors;
