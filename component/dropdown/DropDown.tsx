import React, {ButtonHTMLAttributes, DetailedHTMLProps, FC, PropsWithChildren, useState} from "react";
import OutsideAlerter from "../mouse/OutsideAlerter";
import Logger from "trunks-core-js/logger/logger";

interface IDropDownProps {
  buttonClassName?: string;
  className?: string;
  content: React.ReactNode;
  disabled?: boolean;
  children: React.ReactElement<IDropDownButtonProps> | React.ReactElement<IDropDownButtonProps>[];
}

const DropDown: FC<IDropDownProps> = ({buttonClassName, className, content, disabled, children}) => {
  const [isOpen, setIsOpen] = useState(false);

  const closeDropDown = () => {
    setIsOpen(false);
  }

  return (
    <div className={className}>
      <OutsideAlerter onClick={closeDropDown}>
        <div className="trunks-dropdown">
          <button
            className={["button button-auto", buttonClassName].filter(i => i).join(" ")}
            type="button"
            onClick={() => setIsOpen(prevState => !prevState)}
            disabled={disabled ?? false}
          >
            {content}
          </button>

          <div className="trunks-dropdown-content" style={{display: isOpen ? "block" : "none"}}>
            {React.Children.map(children, (child) => {
              if (!React.isValidElement(child)) {
                Logger.e("Children of a DropDown must be a valid React Element (type '" + typeof child + "' not allowed)");
                return null;
              }

              // Clone child with overridden onClick (close the dropdown after click)
              return React.cloneElement(child, {
                onClick: (event: any) => {
                  child.props.onClick?.(event);
                  closeDropDown();
                }
              });
            })}
          </div>
        </div>
      </OutsideAlerter>
    </div>
  );
}

interface IDropDownButtonProps extends PropsWithChildren<DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>> {
}

export const DropDownButton: FC<IDropDownButtonProps> = ({children, ...otherProps}) => {
  return (
    <button className="button button-transparent" {...otherProps}>
      {children}
    </button>
  );
}

export default DropDown;
