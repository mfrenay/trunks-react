import TRow from "../../../../trunks-core-js/model/TRow";
import TList from "../../../../trunks-core-js/model/TList";

// FIXME : interface à renommer
// On doit mieux décrire le fait que cette interface sert aux objets (listes classiquement) qu'ils peuvent donc OUVRIR un TForm

interface ITFormProps {
    /**
     * FIXME
     * Called when the modal Form needs to open.
     *
     * Can be called on for a new row (when tRow argument is undefined) or to update an existing row (when tRow argument is passed)
     *
     * @param jsEvent Javascript mouse event
     * @param tRow TRow for which a TForm must be opened
     */
    onNewForm?: (jsEvent: any) => void,

    onEditForm?: (jsEvent: any, tList: TList, tRow?: TRow, listAction? : ListActionType) => void,

    tFormComponent: any,

    tFormDataSourceId: string,
}
export type ListActionType = string | null;
export const ACTION_EDIT:ListActionType = "EDIT";
export const ACTION_DUPLICATE:ListActionType = "DUPLICATE";

export default ITFormProps;
