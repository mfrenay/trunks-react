import React, {ComponentPropsWithRef, PropsWithChildren} from "react";

import Logger from "../../../../trunks-core-js/logger/logger";
import TForm from "../../../../trunks-core-js/model/TForm";
import TRow from "../../../../trunks-core-js/model/TRow";
import TList from "../../../../trunks-core-js/model/TList";

import Modal from "../Modal";
import {ACTION_DUPLICATE, ListActionType} from "./ITFormProps";
import TField from "trunks-core-js/model/TField";

function withModalTForm(WrappedComponent: any, TFormComponent: any) {

    interface IProps extends PropsWithChildren<any>, ComponentPropsWithRef<any> {
        tFormDataSourceId: string,
        tFormRowParam?: TRow,
        onCreateFromForm?: (tForm: TForm) => void,
        onUpdateFromForm?: (tForm: TForm) => void,
        onError?: (error: any) => void,
    }

    interface IState {
        showModalTForm: boolean,
        listAction?: ListActionType,
        tFormPrimaryKeys?: TField[]
    }

    class WithModalTForm extends React.Component<IProps, IState> {
        constructor(props: any) {
            super(props);

            this.state = {
                showModalTForm: false,
                listAction: null,
            };
        }

        /**
         * Called when the modal Form needs to open.
         *
         * Can be called on for a new row (when tRow argument is undefined) or to update an existing row (when tRow argument is passed)
         *
         * @param event Javascript mouse event
         * @param tList
         * @param tRow TRow for which a TForm must be open thanks to the current tFormDataSourceId
         * @param listAction
         */
        handleOpenForm = (event: any, tList: TList, tRow?: TRow, listAction? : ListActionType) => {
            event?.preventDefault();

            let primaryKeys: TField[] = [];

            if (tRow != null) {
                try {
                    primaryKeys = tRow.getPrimaryKeyFields(tList.baseRow)!;
                } catch (error) {
                    const msg = `Primary key not found for tRow '${tRow}'`;
                    Logger.e(msg, error);
                    throw new Error(msg);
                }
            }

            this.setState({
                showModalTForm: true,
                listAction: listAction,
                tFormPrimaryKeys: primaryKeys
            });
        }

        /**
         * onSaveModalTForm
         *
         * Callback : function to call if user clicks on "Save" button of the Form modal.
         *            This callback is only called if the validation passed
         */
        onSaveModalTForm = (tForm: TForm) => {
            // We must first close the modal TForm before calling the callbacks,
            // otherwise the component would have been closed by the callbacks.
            // Thus, closing the modal TForm after closing the wrapped component throw an error
            // (ie, 'Warning: Can't perform a React state update on an unmounted component')
            this.closeModalTForm();

            if (this.state.tFormPrimaryKeys && this.state.tFormPrimaryKeys.length > 0
                && this.state.listAction !== ACTION_DUPLICATE
            ) {
                this.props.onUpdateFromForm?.(tForm);
            }
            else {
                this.props.onCreateFromForm?.(tForm);
            }
        }

        /**
         * onCancelModalTForm
         *
         * Callback : function to call if user clicks on "Cancel" button of the Form modal
         */
        onCancelModalTForm = () => {
            this.closeModalTForm();
        }

        /**
         * closeModalTForm
         *
         * Hide form modal window and change state.
         */
        closeModalTForm = () => {
            this.setState({
                showModalTForm: false,
                tFormPrimaryKeys: undefined
            });
        }

        modalShowTForm = () => {
            if (this.state.showModalTForm) {
                if (this.props.tFormDataSourceId == null) {
                    const msg = "TForm data source ID is undefined";
                    Logger.e(msg);
                    throw new Error(msg);
                }

                return (
                    <Modal id="modalTForm" className="modal-tform" >
                        {/* FIXME: This form must be configurable (Generic form or specific form) */}
                        <TFormComponent
                            dataSourceId={this.props.tFormDataSourceId}
                            tRowParams={this.props.tFormRowParam}
                            primaryKeys={this.state.tFormPrimaryKeys}
                            listAction={this.state.listAction}
                            onSave={this.onSaveModalTForm}
                            onCancel={this.onCancelModalTForm}
                            onError={this.props.onError}
                        />
                    </Modal>
                );
            }

            return null;
        }

        render() {
            const {forwardedRef, ...otherProps} = this.props;

            return (
                <>
                    <WrappedComponent
                        onNewForm={this.handleOpenForm}
                        onEditForm={this.handleOpenForm}
                        tFormComponent={TFormComponent}

                        ref={forwardedRef}

                        {...otherProps}
                    />
                    {this.modalShowTForm()}
                </>
            );
        }
    }

    // @ts-ignore
    WithModalTForm.displayName = `${WithModalTForm.name}(${getDisplayName(WrappedComponent)})`;

    return React.forwardRef((props: any, ref) => {
        return <WithModalTForm
                    {...props}
                    forwardedRef={ref}
                /> ;
    });
}

function getDisplayName(WrappedComponent: any) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withModalTForm;
