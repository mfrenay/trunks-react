import {Component, ComponentProps, ReactNode} from "react";
import {createPortal} from "react-dom";

//import "./Modal.scss";

/**
 * Search after and return a div element with id "Trunks__ModalPortal".
 * If not found in the DOM, create it and append it in document body.
 */
function appendModalDivInBody(): HTMLDivElement | null {
  if (typeof window === "undefined") {
    return null;
  }

  const modalId = "Trunks__ModalPortal";

  // Search for div element with id "Trunks__ModalPortal" in the DOM and return it if found
  let modalPortalDiv = document.getElementById(modalId);
  if (modalPortalDiv && modalPortalDiv instanceof HTMLDivElement) {
    return modalPortalDiv;
  }

  // Create a div with id "Trunks__ModalPortal" and append it to body
  modalPortalDiv = document.createElement("div");
  modalPortalDiv.id = modalId;
  document.body.appendChild(modalPortalDiv);

  return modalPortalDiv as HTMLDivElement;
}

// We get hold of the div with the id modal that we have created in index.html
const modalPortalDiv = appendModalDivInBody();

interface IProps extends ComponentProps<any> {
  id: string,
  header?: ReactNode,
  footer?: ReactNode,
  onClose?: () => void,
  zIndex?: number,
  width?: number | string,
}

interface IState {
}

class Modal extends Component<IProps, IState> {
  private element: HTMLDivElement;

  constructor(props: IProps) {
    super(props);
    // We create an element div for this modal
    this.element = document.createElement("div");
  }

  // We append the created div to the div#modal
  componentDidMount() {
    modalPortalDiv?.appendChild(this.element);

    // Add overflow: hidden on body element
    if (typeof window !== "undefined") {
      if (modalPortalDiv?.children.length === 1) {
        document.body.style.overflow = "hidden";
      }
    }
  }

  /**
   * We remove the created div when this Modal Component is unmounted
   * Used to clean up the memory to avoid memory leak
   */
  componentWillUnmount() {
    modalPortalDiv?.removeChild(this.element);

    // Remove overflow: hidden on body element
    if (typeof window !== "undefined") {
      if (modalPortalDiv?.children.length === 0) {
        document.body.style.overflow = "";
      }
    }
  }

  getModalId() {
    return this.props.id == null ?
      "modalContainer" :
      this.props.id;
  }

  render() {
    const modal = (
      <>
        <div className={"modal-backdrop fade show custom-modal-backdrop modal-backdrop-" + this.getModalId()}
             style={{zIndex: this.props.zIndex && this.props.zIndex - 1}}
        />
        <div className="modal"
             id={this.getModalId()}
             tabIndex={-1}
             role="dialog"
             style={{display: "block", zIndex: this.props.zIndex}}
        >
          <div className={"modal-dialog modal-dialog-centered " + (this.props.className ?? "")}
               style={{width: this.props.width}}
               role="document"
          >
            <div className="modal-content" style={{width: this.props.width}}>
              {this.props.header && (
                <div className="modal-header">
                  {this.props.header}
                </div>
              )}

              <div className="modal-body">
                {this.props.children}
              </div>

              {this.props.footer && (
                <div className="modal-footer">
                  {this.props.footer}
                </div>
              )}
            </div>
          </div>
        </div>
      </>
    );

    return (
      <>
        {createPortal(modal, this.element)}
      </>
    );
  }
}

export default Modal;
