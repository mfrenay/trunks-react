import React, {FC, PropsWithChildren} from "react";
//import "./LoadingWrapper.scss";

interface ILoaderProps {
  color?: string;
  size?: string | number;
}

export const Loader: FC<ILoaderProps> = ({color, size}) => (
  <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px"
       y="0px" width={size ?? 64} height={size ?? 64} viewBox="0 0 100 100" enableBackground="new 0 0 0 0"
       xmlSpace="preserve">
    <path fill={color ?? "currentColor"}
          d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
      <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50"
                        to="360 50 50" repeatCount="indefinite"/>
    </path>
  </svg>
);

const LoadingWrapper: FC<PropsWithChildren<{ isLoading: boolean }>> = ({isLoading, children}) => {
  const styles = isLoading ? {minHeight: "96px"} : {};

  return (
    <div className="loading-wrapper" style={styles}>
      {children}
      {isLoading && (
        <>
          {children && <div className="loading-container loading-container-backdrop"/>}
          <div className="loading-container">
            <Loader/>
          </div>
        </>
      )}
    </div>
  );
}

export default LoadingWrapper;