import {FC} from "react";
import ConfirmModal, {IConfirmModalProps} from "./ConfirmModal";
import {config} from "../../config";

/**
 * Display a Modal to confirm a delete event.
 * The modal contains by default a cancel button and a delete button
 *
 * @param props
 * @constructor
 */
const ConfirmDeleteModal: FC<IConfirmModalProps> = function (props: IConfirmModalProps) {
    const {t} = config;

    return (
        <ConfirmModal yesButtonClassnames={["button-delete-primary"]}
                      yesButtonLabel={t("default.btn_delete")}
                      noButtonLabel={t("default.btn_cancel")}
                      {...props}
        />
    );
}

export default ConfirmDeleteModal;
