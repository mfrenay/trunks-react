import {FC, PropsWithChildren} from "react";
import Modal from "../modal/Modal";
import {config} from "../../config";
import AsyncButton from "../button/AsyncButton";

export interface IConfirmModalProps {
    title?: string,
    subtitle?: string,
    message?: string,
    messageClassnames?: string[],
    onConfirmYes: () => Promise<any> | void,
    onConfirmNo: () => Promise<any> | void,
    yesButtonClassnames?: string[],
    yesButtonLabel?: string,
    noButtonClassnames?: string[],
    noButtonLabel?: string,
    isYesButtonDisabled?: boolean // if true, the yes button will be disabled, otherwise it will be enabled
}

const ConfirmModal: FC<PropsWithChildren<IConfirmModalProps>> = function (props) {
    const {t} = config;

    const handleYes = async () => {
        await props.onConfirmYes();
    }

    const handleNo = async () => {
        await props.onConfirmNo();
    }

    const messageCustomClassName = (props.messageClassnames && props.messageClassnames.length > 0)
        ? props.messageClassnames?.join(" ")
        : ""

    const noButtonCustomClassName = (props.noButtonClassnames && props.noButtonClassnames.length > 0)
        ? props.noButtonClassnames?.join(" ")
        : "button-secondary";

    const yesButtonCustomClassName = (props.yesButtonClassnames && props.yesButtonClassnames.length > 0)
        ? props.yesButtonClassnames?.join(" ")
        : "button-primary";

    return (
        <Modal id="modalConfirm">
            <div>
                { props.title &&
                    <div className="modal-header d-block">
                        <h2>{props.title}</h2>
                        { props.subtitle &&
                            <h3>{props.subtitle}</h3>
                        }
                    </div>
                }
                <div className="modal-container">
                    {props.message && <p className={messageCustomClassName}>{props.message}</p>}
                    {props.children}
                </div>
                <div className="modal-footer">
                    <div className="container-fluid p-none">
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <AsyncButton
                                  className={`button ${noButtonCustomClassName}`}
                                  onClick={handleNo}
                                >
                                    {props.noButtonLabel ?? t("default.btn_no")}
                                </AsyncButton>
                            </div>
                            <div className="col-12 col-md-6">
                                <AsyncButton
                                  className={`button ${yesButtonCustomClassName}`}
                                  onClick={handleYes}
                                  disabled={!!props.isYesButtonDisabled}
                                >
                                    {props.yesButtonLabel ?? t("default.btn_yes")}
                                </AsyncButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}

export default ConfirmModal;
