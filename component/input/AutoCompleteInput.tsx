import * as React from "react";

import {AllTypeaheadOwnAndInjectedProps, AsyncTypeahead} from "react-bootstrap-typeahead";

import TRow from "../../../trunks-core-js/model/TRow";
import Logger from "../../../trunks-core-js/logger/logger";
import StringUtils from "../../../trunks-core-js/utilities/StringUtils";
import ErrorManager from "../../ui/ErrorManager";
import TrunksWebService from "../../../trunks-core-js/web-service/TrunksWebService";
import {config} from "../../config";

const AC_LABEL_FIELD_ID = "LABEL";

interface IProps {
    id: string,
    placeholder?: string,
    defaultSelected: any[],

    // currentText is the text that is written in the input
    // substitutedText is the result of the customSubstituteQueryCharacters function applied to the input text
    onChange: (selected: Array<any|string>, currentText: string, substitutedText: string) => void,

    dataSourceId: string,
    tRowParams?: TRow,
    options?: any[],
    minLength?: number
    searchOnlyStartingWith: boolean,
    customSubstituteQueryCharacters?: (text: string) => string,
    newItemLinkLabel?: string
}

interface IState {
    isLoading: boolean,
    options: any[],
    isValidOption: boolean,
    currentText: string,
    currentSubstitutedText: string,
    isOpen: boolean,
}

class AutoCompleteInput extends React.Component<IProps, IState> {
    private readonly acInputRef: React.RefObject<any>;


    constructor(props: IProps) {
        super(props);

        this.acInputRef = React.createRef();

        const defaultOptions = this.props.options ?? [];

        this.state = {
            isLoading: false,
            options: defaultOptions,
            isValidOption: this.props.defaultSelected.length > 0,
            currentText: "",
            currentSubstitutedText: "",
            isOpen: false,
        }
    }

    focus = () => {
        this.acInputRef?.current?.focus();
    }

    handleSearch = (query: string) => {
        if (StringUtils.getNString(this.props.dataSourceId) !== "") {
            Logger.d("AutoCompleteInput.handleSearch - query: '" + query + "'");
            let substitutedText = query;
            if (this.props.customSubstituteQueryCharacters) {
                substitutedText = this.props.customSubstituteQueryCharacters(query);
                Logger.d("AutoCompleteInput.handleSearch - query text after custom substitutions: '" + query + "'");
            }

            this.setState({isLoading: true, currentText: query, currentSubstitutedText: substitutedText, isOpen: true});

            const dataWebService = new TrunksWebService();
            dataWebService.getMatchingValues((resultsJSON) => {
                    let options = resultsJSON ?? [];
                    // mich -21/01/2021
                    Logger.d("<<<< AutoCompleteInput.handleSearch.getMatchingValues - resultsJSON = ", resultsJSON);
                    Logger.d("<<<< AutoCompleteInput.handleSearch.getMatchingValues - options.length = " + options.length);

                    // Add "New Item" option at the end of the options list
                    if (this.props.newItemLinkLabel) {
                        options = [...options, {TYPE: "ACTION", ACTION: "NEW", LABEL: this.props.newItemLinkLabel}];
                    }

                    this.setState({options: options, isLoading: false});
                }
                , (error) => {
                    ErrorManager.displayErrorMessage(error);
                }, this.props.dataSourceId, this.props.searchOnlyStartingWith, substitutedText, this.props.tRowParams)
                .catch(error => {
                    Logger.e("<<<<<<< AutoCompleteInput.handleSearch.error...", error);
                    this.setState(() => {
                        throw error;
                    });
                });

        }
    };

    handleChange = (selected: Array<any | string>) => {
        const isValid = selected.length > 0;
        this.setState({isValidOption: isValid, isOpen: false});

        //this.props.onChange(selected, this.acInputRef.current.search);
        this.props.onChange(selected, this.state.currentText, this.state.currentSubstitutedText);
    }

    render() {
        const {t} = config;
        const {id, minLength, placeholder, defaultSelected} = this.props;

        return (
            <AsyncTypeahead
                ref={this.acInputRef}
                id={id}
                labelKey={AC_LABEL_FIELD_ID}
                placeholder={placeholder ?? t("auto_complete_input.ac_input_placeholder")}
                isLoading={this.state.isLoading}
                clearButton={true}
                minLength={minLength ?? 1}
                delay={500}
                promptText={t("auto_complete_input.ac-input_prompt_text")}
                searchText={t("auto_complete_input.ac_input_search_text")}
                emptyLabel={t("auto_complete_input.ac_input_no_result")}
                options={this.state.options}
                defaultSelected={defaultSelected}
                selected={defaultSelected} // Used to force re-rendering because defaultSelected props is used only once
                filterBy={(option: any, props: AllTypeaheadOwnAndInjectedProps<any>) => {
                    // Filter applied on options already loaded from the server
                    // This is useful to override the default implementation of filterBy that filters with a contain(<txt>) and not a startWith
                    //return stringUtils.replaceSpecialChars(option._label.toLowerCase()).startsWith(stringUtils.replaceSpecialChars(props.text.toLowerCase()));
                    return true;
                }}
                open={this.state.isOpen}
                onFocus={() => this.setState({isOpen: this.state.options.length > 0})}
                onSearch={this.handleSearch}
                onChange={this.handleChange}
                onBlur={() => {
                    if (!this.state.isValidOption)
                        this.acInputRef.current.clear();
                    this.setState({isOpen: false})
                }}
                /*renderMenuItemChildren={(option: any, props) => (
                    <div>
                        <span>{option.label}</span>
                    </div>
                )}*/
            />
        );
    }
}

export default AutoCompleteInput;
export {AC_LABEL_FIELD_ID};
