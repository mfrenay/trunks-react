import * as React from "react";
import {useEffect, useState} from "react";
import DateUtils from "../../../trunks-core-js/utilities/DateUtils";

interface IDurationInputProp extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    duration: number,
    onChangeDuration?: (time: number) => void
}

function DurationInput({duration, onChangeDuration, ...otherProps}: IDurationInputProp) {
    const [durationStr, setDurationStr] = useState(DateUtils.timeToHoursString(duration) ?? "");

    useEffect(() => {
        setDurationStr(DateUtils.timeToHoursString(duration) ?? "");
    }, [duration]);

    //const durationTime: number = useMemo(() => dateUtils.hoursStringToTime(durationStr) ?? 0, [durationStr]);

    return (
        <input {...otherProps}
               maxLength={5}
               type="text"
               placeholder={"HH:MM"}
               value={durationStr}
               onChange={event => {
                   const newDuration = event.target.value;

                   // Check if the content is in the right format (HH:MM)
                   const hoursMin = newDuration.split(":");
                   const hoursRegex = new RegExp(/^$|^(\d|[01][0-9]|2[0-3])$/);
                   const minutesRegex = new RegExp(/^$|^(\d|[0-5][0-9])$/);

                   if (hoursMin.length === 1 && !hoursRegex.test(hoursMin[0])) {
                       return null;
                   }
                   else if (hoursMin.length === 2 && (!hoursRegex.test(hoursMin[0]) || !minutesRegex.test(hoursMin[1]))) {
                       return null;
                   }

                   setDurationStr(newDuration);
                   const time = DateUtils.hoursStringToTime(newDuration);

                   if (time != null) {
                       onChangeDuration?.(DateUtils.hoursStringToTime(newDuration));
                   }
               }}
               onBlur={_ => {
                   // When we leave the component, if the format is not correct, reset the content with the duration props
                   const time = DateUtils.hoursStringToTime(durationStr);

                   if (time == null) {
                       setDurationStr(DateUtils.timeToHoursString(duration) ?? "");
                   }
               }}
        />
    );
}

export default DurationInput;
