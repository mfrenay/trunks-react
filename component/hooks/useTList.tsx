import {useCallback, useEffect, useState} from "react";
import TList from "trunks-core-js/model/TList";
import TrunksWebService from "trunks-core-js/web-service/TrunksWebService";
import TRow from "trunks-core-js/model/TRow";

const useTList = (listId: string, options?: {params?: TRow}) => {
  const [tList, setTList] = useState<TList | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  const fetchList = useCallback(async () => {
    setIsLoading(true);

    const ws = new TrunksWebService();
    await ws.retrieveListAndData((tList: TList) => {
      setTList(tList);

      // FIXME: Change this
      setIsLoading(false);
    }, null, listId, {params: options?.params});
  }, [listId]);

  useEffect(() => {
    fetchList();
  }, [fetchList]);

  return {
    tList,
    setTList,
    isLoading
  }
}

export default useTList;