import React, {Fragment, useCallback, useEffect, useState} from "react";

import TList from "../../../trunks-core-js/model/TList";
import TRow from "../../../trunks-core-js/model/TRow";
import TField from "../../../trunks-core-js/model/TField";
import Logger from "../../../trunks-core-js/logger/logger";
import StringUtils from "../../../trunks-core-js/utilities/StringUtils";
import TDeleteRowDTO from "../../../trunks-core-js/web-service/dto/TDeleteRowDTO";
import TrunksWebService from "../../../trunks-core-js/web-service/TrunksWebService";

import ITFormProps from "../modal/wrapper/ITFormProps";
import ListField from "../list/field/ListField";

import ConfirmDeleteModal from "../confirm/ConfirmDeleteModal";

import {config} from "../../config";
import ErrorManager from "../../ui/ErrorManager";

import TFilterListDTO from "trunks-core-js/web-service/dto/TFilterListDTO";
import TSortListDTO from "trunks-core-js/web-service/dto/TSortListDTO";
import TFilterAndSortListDTO from "trunks-core-js/web-service/dto/TFilterAndSortListDTO";
import GenericValidationUtils, {IListError} from "../../utilities/GenericValidationUtils";
import FormErrors from "../error/FormErrors";
import AlertManager from "../../ui/AlertManager";
import NumberUtils from "trunks-core-js/utilities/NumberUtils";
import AdvancedSearch, {AdvancedSearchHandler} from "../search/AdvancedSearch";
import TInsertRowDTO from "trunks-core-js/web-service/dto/TInsertRowDTO";
import TranslationUtils from "../../utilities/TranslationUtils";
import ListErrors from "../error/ListErrors";
import TListHelper from "trunks-core-js/helper/TListHelper";

type IDataTableProps = IProps | (IProps & ITFormProps);

export enum ActionPosition {
    LEFT,
    RIGHT,
}

export interface IDataTableRowProps
{
    // If defined: the datatable will display a column to select a row.
    // A call on the button in this column will call this callback.
    onSelectItem?: ((tRow: TRow) => void),

    // If defined: the datatable will execute a method when a row is clicked.
    // A click on a row will call this callback.
    onClickRow?: ((tRow: TRow) => void),

    // If defined: the datatable will execute a method when a button or an image of a row is clicked.
    // A click on a button or an image of a row will call this callback.
    onRowFieldAction?: ((tRow: TRow, tList: TList, action: string) => void),

    // method that can be defined in the specific component e.g. {Entity}List.tsx
    // to write some logic to allow edit or not for a given row
    // if false, the edit button will be disabled
    isRowEditable?: (tList: TList, tRow: TRow) => boolean,

    // If true, the datatable will display a column to delete a row.
    // A click on the button in this column will call this callback.
    enableDelete: boolean,

    // method that can be defined in the specific component e.g. {Entity}List.tsx
    // to write some logic to allow delete or not for a given row
    // if false, the delete button will be disabled
    isRowDeletable?: (tList: TList, tRow: TRow) => boolean,

    // If true, the pagination row (nb items per page select + pagination component) will not be displayed
    // The whole tList will be displayed
    hidePagination?: boolean,
}

interface IProps extends IDataTableRowProps {
    // The TList to display.
    tList: TList,

    // Params given to the web service (only delete at the moment)
    tRowParams?: TRow,

    // Callback to change the tList
    setTList: (tList: TList) => void,

    // Callback to get filter values and params
    getTFilterListDTO?: () => TFilterListDTO,

    // Callback called with the tRow of the TForm that has just been saved
    // FIXME: A déplacer dans ITFormProps et remplacer tRow par un TForm
    // onDidUpdateTForm?: ((tRow: TRow) => void),

    // Callback called when a row has been insert
    onDidInsertItem?: ((tRow: TRow) => void),

    // Callback called when a row has been deleted
    onDidDeleteItem?: ((tRow: TRow, nbDeletedRows: number) => void),

    refetch?: () => void,

    // Position of the action columns (LEFT or RIGHT)
    actionsPosition?: ActionPosition,

    // CSS classes added to top-level element (wrapper)
    className?: string,
}

interface IState {

    //////// PAGINATION ////////
    /**
     * Index (zero-based) of the page currently displayed.
     */
    currentPage: number,
    /**
     * Number of rows to display per page.
     */
    rowsPerPage: number,

    //////// DELETE ////////
    /**
     * Boolean value indicating whether the confirm delete modal is shown. True if showed, false otherwise.
     */
    showModalConfirmDelete: boolean,
    /**
     * The TRow of the row on which the delete button was clicked.
     */
    deletingRow: TRow | null,

    //////// ERRORS ////////
    /**
     * A string corresponding to the current error.
     * A null value means that there is currently no error.
     */
    errorMessage: string | null,

    listErrors: IListError[] | null,

    expandedRowIndexes: string[],
}

const ROWS_PER_PAGE = [5, 10, 15, 20, 25, 50];

/**
 * Display a TList in a data table and gives the possibility to make action on each TRow of the TList.
 *
 * Action types:
 *  - "select" and "delete"
 *  - "update" via a TForm
 *
 *  FIXME: This comment below will be modified later (talk about Routing)
 *  This component is usually wrapped with HOC withModalTForm to give the possibility to open a TForm directly from the DataTable
 */
class DataTable extends React.Component<IDataTableProps, IState> {

    // Setup with -1, like this on the first sort event,
    // it will be switched to 1 that will make the sorting ASC
    static INITIAL_SORTING_ORDER = -1;

    // Property that contains all refs of AdvancedSearch corresponding to sublists that are mounted.
    // Useful to give to custom list fields a handler to refetch the sublist
    private sublistRefs: Record<string, any>;

    constructor(props: IDataTableProps) {
        super(props);

        this.sublistRefs = {};

        this.state = {
            currentPage: 0,
            rowsPerPage: this.props.tList.pageSize,
            showModalConfirmDelete: false,
            deletingRow: null,
            errorMessage: null,
            listErrors: null,
            expandedRowIndexes: []
        }
    }

    componentDidMount() {
        Logger.d("DataTable.componentDidMount...");

        if (!this.props.tList.isDBSort) {
            if (this.props.tList.sortingFieldId == null) {
                const defaultSortingFieldID = this.props.tList.customObjectParams?.DEFAULT_SORTING_FIELD;

                if (StringUtils.getNString(defaultSortingFieldID) !== "") {
                    this.sortList(this.props.tList.baseRow.getFieldById(defaultSortingFieldID!)!);
                }
            }
        }
    }

    componentDidUpdate(prevProps: Readonly<IDataTableProps>, prevState: Readonly<IState>, snapshot?: any) {
        // If the list is not yet sorted, sort it based on DEFAULT_SORTING_FIELD (if given)
        if (!this.props.tList.isDBSort && this.props.tList.sortingFieldId == null) {
            const defaultSortingFieldID = this.props.tList.customObjectParams?.DEFAULT_SORTING_FIELD;

            if (StringUtils.getNString(defaultSortingFieldID) !== "") {
                this.sortList(this.props.tList.baseRow.getFieldById(defaultSortingFieldID!)!);
            }
        }
    }

    handleClickItem(row: TRow) {
        this.props.onSelectItem!(row);
    }

    handlePageClick = (page: number) => {
        this.setState({
            currentPage: page
        });
    }

    handleDeleteItem = (tRow: TRow) => {
        this.setState({
            showModalConfirmDelete: true,
            deletingRow: tRow,
        });
    }

    /**
     * modalConfirmDelete
     *
     * Display a modal window with a message to confirm or not the deletion
     */
    modalConfirmDelete() {
        const {t} = config;
        if (this.state.showModalConfirmDelete) {
            return (
                <ConfirmDeleteModal message={t("data_table.msg_confirm_delete")}
                                    onConfirmYes={this.confirmDeleteYes}
                                    onConfirmNo={this.cancelDelete}/>
            );
        }

        return null;
    }

    /**
     * confirmDeleteYes
     *
     * Callback : function called when user clicks to confirm Delete in the popup
     * Calls callback onDelete from the parent
     */
    confirmDeleteYes = () => {
        const {t} = config;

        // Construct the tDeleteRowDTO to give to the web service function
        const primaryKeyFields = this.state.deletingRow!.getPrimaryKeyFields(this.props.tList.baseRow);

        // If at least one pk field has no value
        if (primaryKeyFields.findIndex(pkField => pkField.stringValue === "") !== -1) {
            // Delete the row locally
            const tList = Object.assign(new TList("", new TRow()), this.props.tList);
            tList.rows = tList.rows.filter(row => row.nr !== this.state.deletingRow!.nr);

            this.props.setTList(tList);

            this.setState({
                showModalConfirmDelete: false,
                deletingRow: null
            });

            return;
        }

        // Delete the row on the server with an API call
        const tDeleteRowDTO = new TDeleteRowDTO({
            rowPkFields: new TRow(primaryKeyFields),
            params: this.props.tRowParams
        })

        const dataWebService = new TrunksWebService();
        dataWebService.deleteRow((nbDeletedRows) => {

            // If there is a callback props 'onDidDeleteItem', the delete is the responsibility of this callback
            if (this.props.onDidDeleteItem) {
                this.props.onDidDeleteItem(this.state.deletingRow!, nbDeletedRows);
            }
            // If there is no callback props 'onDidDeleteItem', remove directly the row from tList in state
            else {
                const tList = Object.assign(new TList("", new TRow()), this.props.tList);
                tList.rows = tList.rows.filter(row => row.nr !== this.state.deletingRow!.nr);

                this.props.setTList(tList);
            }

            this.setState({
                showModalConfirmDelete: false,
                deletingRow: null
            });

            AlertManager.showSuccessToast(t("data_table.msg_delete_success"));

        }, (error: any) => {
            // Display error
            this.setState({
                errorMessage: TranslationUtils.getErrorTranslation(t, error.message?.toLowerCase()),
                showModalConfirmDelete: false,
                deletingRow: null
            });
        }, this.props.tList.id, tDeleteRowDTO)
        .catch(error => {
            Logger.e("<<<<<<< DataTable.confirmDeleteYes.error...", error);
            this.setState(() => { throw error; });
        });
    }

    /**
     * cancelDelete
     *
     * Callback : function called when user answers No in the popup
     * Change state and so the modal window will disappear
     */
    cancelDelete = () => {
        this.setState({
            showModalConfirmDelete: false,
            deletingRow: null
        });
    }

    /**
     * refreshList
     */
    refreshList = (tList: TList) => {
        this.props.setTList(tList);
    }

    /**
     * displayNumberOfRows
     * @private
     */
    private displayNumberOfRows() {
        const {t} = config;
        const {tList} = this.props;
        const {currentPage, rowsPerPage} = this.state

        return <>
            {(StringUtils.stringToBoolean(tList.customObjectParams?.SHOW_NB_ROWS)) && (
              <div className="text-center my-2" style={{fontSize: "var(--f-s)"}}>
                  {t("data_table.label_nb_rows", {
                      indexFirst: (currentPage * rowsPerPage) + 1,
                      indexLast: Math.min(((currentPage + 1) * rowsPerPage), tList.rows.length),
                      nbTotalRows: tList.rows.length,
                  })}
              </div>
            )}

            {(tList.isMaxRowToRetrieveReached && (tList.maxRowToRetrieve > tList.limitMaxRowToRetrieve)) && (
              <div className="text-center my-2" style={{fontSize: "var(--f-s)"}}>
                  {t("data_table.label_max_row_reached", {
                      p1: tList.limitMaxRowToRetrieve,
                      p2: tList.maxRowToRetrieve
                  }) + " "}

                  <i className="fas fa-info-circle" data-toggle="tooltip"
                     title={t("data_table.label_max_row_reached_use_filter")}/>
              </div>
            )}
        </>
    }

    /**
     * paginateButtons
     */
    private paginateButtons(): any[] {
        const {currentPage} = this.state;
        const {t} = config;
        const nbPages = Math.ceil(this.props.tList.rows!.length/this.state.rowsPerPage);
        const buttons = [];

        // "Previous" button
        buttons.push(
            <li key={0} className={"paginate_button page-item previous " + (currentPage <= 0 ? "disabled" : "")}>
                <button className="page-link"
                        onClick={jsEvent => {
                            jsEvent.preventDefault();

                            if (currentPage > 0) {
                                this.setState({currentPage: currentPage - 1});
                            }
                        }}
                >
                    {t("data_table.btn_previous")}
                </button>
            </li>
        );

        // Page number buttons
        const nbPageInPaging = 5;
        const indexForCurrent = Math.floor(nbPageInPaging/2)
        const iInitial = (currentPage + indexForCurrent >= nbPages) ?
          Math.max(nbPages - nbPageInPaging, 0)
          : Math.max(currentPage - indexForCurrent, 0);

        for (let i = iInitial; i < Math.min(iInitial + nbPageInPaging, nbPages); ++i) {
            buttons.push(
                <li key={i+1} className={"paginate_button page-item " + (i === this.state.currentPage ? "active" : "")}>
                    <button className="page-link"
                            onClick={(event => {
                                event.preventDefault();
                                this.handlePageClick(i);
                            })}
                    >
                        {i + 1}
                    </button>
                </li>
            );
        }

        // "Next" button
        buttons.push(
            <li key={nbPages+1} className={"paginate_button page-item next " + (currentPage >= (nbPages) - 1 ? "disabled" : "")}>
                <button className="page-link"
                        onClick={jsEvent => {
                            jsEvent.preventDefault();

                            if (currentPage < nbPages - 1) {
                                this.setState({currentPage: currentPage + 1});
                            }
                        }}
                >
                    {t("data_table.btn_next")}
                </button>
            </li>
        );

        return buttons
    }

    private rowsPerPageSelect(): any {
        return (
            <select name="select_row_per_page"
                    className="custom-select custom-select-sm form-control form-control-sm"
                    defaultValue={this.state.rowsPerPage}
                    onChange={event => {
                        event.preventDefault();

                        this.setState({
                            rowsPerPage: Number(event.target.value),

                            // Reset the currentPage to the first page to avoid being
                            // on a page that does not exist
                            currentPage: 0,
                        });
                    }}
            >
                {
                    [...ROWS_PER_PAGE, this.props.tList.pageSize]
                      .filter((item, index, arr) => arr.indexOf(item) === index)
                      .sort((a, b) => a - b)
                      .map((item: number) => {
                        return <option value={item} key={item} >{item}</option>
                    })
                }
            </select>
        );
    }

    // FIXME: give this.props.actionsPosition a default value
    getActionPosition() {
        return this.props.actionsPosition ?? ActionPosition.RIGHT;
    }

    /**
     * Go back to the first page of the datatable
     */
    goToFirstPage() {
        this.setState({currentPage: 0});
    }

    /**
     * Sort the tList on the given field
     *
     * @param field The TField on which to sort the list
     * @private
     */
    private sortList(field: TField) {
        const {tList} = this.props;
        const newSortingOrder = tList.sortingFieldId === field.id ? tList.sortingOrder * -1 : 1;

        if (tList.isDBSort) {
            const tFilterAndSortListDto = new TFilterAndSortListDTO({
                tFilterListDTO: this.props.getTFilterListDTO?.(),
                tSortListDTO : new TSortListDTO({sortingFieldId : field.id, sortingOrder: newSortingOrder})
            });

            // call web service
            const dataWebService = new TrunksWebService();
            dataWebService.filterAndSortList((tListFilteredAndSorted: TList) => {
                // Be careful: if ONLY_SEND_ROWS_ON_FILTER="Y" in the list.xml data source definition,
                // the tListFilteredAndSorted will contain only rows (and not the base row)
                // Thus, we need to keep the current base row
                  if (StringUtils.stringToBoolean(tListFilteredAndSorted.customObjectParams?.ONLY_SEND_ROWS_ON_FILTER ?? "true")) {
                      tListFilteredAndSorted.baseRow = tList.baseRow;
                  }

                  this.props.setTList(tListFilteredAndSorted);
                  this.goToFirstPage();
            }
            , (error) => {
                Logger.e("DataTable.sortList - filterAndSortList(" + tList.id + ")... ", error);
                ErrorManager.displayErrorMessage(error);
            }
            , tList.id, tFilterAndSortListDto)
            .catch(error => {
                this.setState(() => { throw error; });
            });
        }
        else {
            tList.sort(field, newSortingOrder);
            this.props.setTList(Object.assign(new TList("", new TRow()), tList));
        }
    }

    /**
     * handleSave
     * @param event
     */
    async saveList(event?: any) {
        Logger.d("AdvancedSearch.handleSave...");
        event?.preventDefault();

        const {tList} = this.props;
        if (tList == null) {
            return;
        }

        // Generic validation of the TList
        const listErrors = GenericValidationUtils.validateList(tList, {maxNbRowCheck: 2});
        this.setState({listErrors});

        if (listErrors.length > 0) {
            return;
        }

        const rowsToSend: TRow[] = [];

        tList.rows.forEach((tRow) => {
            const isNewOrModifiedRow = TListHelper.setRowState(tRow, tList.baseRow);

            if (isNewOrModifiedRow) {
                rowsToSend.push(tRow);
            }
        });

        const hasListChanged = rowsToSend.length > 0;

        // If no modification => display warning
        if (!hasListChanged) {
            AlertManager.showWarningToast(config.t("trunks:default.msg_warning.save_data_not_changed"));
            return;
        } else {
            // Save list
            const dataWebService = new TrunksWebService();
            dataWebService.saveList(() => {
                AlertManager.showSuccessToast(config.t("advanced_search.msg_save_success"));
                this.setState({
                    errorMessage: null,
                    listErrors: null
                });

            }, (error) => {
                Logger.e("<<<<<<< AdvancedSearch.handleSave.callback.error...", error);
                ErrorManager.displayErrorMessage(error);
            }, tList.id, new TList("", new TRow(), rowsToSend), this.props.tRowParams)
              .catch(error => {
                  Logger.e("<<<<<<< AdvancedSearch.handleSave.finalizeSave.error...", error);
                  ErrorManager.displayErrorMessage(error);
              });
        }
    }


    // -----------------------------------------------------------------------------------------------------------------
    // ------ RENDER
    // -----------------------------------------------------------------------------------------------------------------

    render() {
        const {t} = config;
        const {tList} = this.props;

        // The list is empty and we can not add rows with AddRow footer
        if (tList.rows.length === 0 && !tList.hasAddFooter) {
            return <p>{t("data_table.label_no_result")}</p>
        }

        const {currentPage, rowsPerPage} = this.state;
        const pagedTList: TRow[] = this.props.hidePagination ?
          tList.rows
          : tList.rows!.slice((currentPage * rowsPerPage), ((currentPage + 1) * rowsPerPage));

        // position: The position to match to return the header
        const actionsHeader = (position: ActionPosition) => {
            const actionsHeader = [];

            if (position === this.getActionPosition()) {
                // Display "update" button only if the onEditForm callback has been defined
                if ((this.props as ITFormProps).onEditForm) {
                    actionsHeader.push(<th key="edit-row" scope="col"/>);
                }

                // Display "delete" button only if the props enableDelete is true
                if (this.props.enableDelete) {
                    actionsHeader.push(<th key="delete-row" scope="col"/>);
                }
            }

            return actionsHeader;
        }

        // position: The position to match to return the body
        const actionsBody = (position: ActionPosition, row: TRow) => {
            const actionsBody = [];

            if (position === this.getActionPosition()) {
                // Display "update" button only if the onEditForm callback has been defined
                // Remark: onEditForm props is given by the HOC withModalTForm OR directly to the DataTable component
                if ((this.props as ITFormProps).onEditForm) {
                    // isRowDeletable is normally defined in the specific component e.g. {Entity}List.tsx
                    if (this.props.isRowEditable?.(tList, row) === false) {
                        actionsBody.push(
                            <td className="list_field_action -edit" key="edit-row">
                                <button className="button button-transparent button-np button-auto btn-disabled" disabled>
                                    <i className="fas fa-edit"/>
                                </button>
                            </td>
                        );
                    }
                    else {
                        actionsBody.push(
                            <td className="list_field_action -edit" key="edit-row">
                                <button className="button button-transparent button-np button-auto"
                                        onClick={(e) => (this.props as ITFormProps).onEditForm?.(e, tList, row)}>
                                    <i className="fas fa-edit"/>
                                </button>
                            </td>
                        );
                    }
                }

                // Display "delete" button only if the props enableDelete is true
                if (this.props.enableDelete) {
                    // isRowDeletable is normally defined in the specific component e.g. {Entity}List.tsx
                    if (this.props.isRowDeletable?.(tList, row) === false) {
                        actionsBody.push(
                            <td className="list_field_action -delete" key="delete-row">
                                <button className="button button-transparent button-np button-auto btn-disabled" disabled>
                                    <i className="fas fa-trash"/>
                                </button>
                            </td>
                        );
                    }
                    else {
                        actionsBody.push(
                            <td className="list_field_action -delete" key="delete-row">
                                <button className="button button-transparent button-np button-auto"
                                        onClick={(_) => this.handleDeleteItem(row)}>
                                    <i className="fas fa-trash"/>
                                </button>
                            </td>
                        );
                    }
                }
            }

            return actionsBody;
        }

        const subLists = tList.customObjectParams?.SUB_LIST?.split(",").filter(s => s) ?? [];
        const isRowExpandable = subLists.length > 0;
        const {i18n} = config;

        return (
            <>
                <ListErrors listErrors={this.state.listErrors}/>

                <div className={["dataTables_wrapper", this.props.className].filter(x => x).join(" ")}>
                    {this.state.errorMessage && (
                        <div className="alert alert-danger alert-dismissible fade show mt-xs" role="alert">
                            {this.state.errorMessage}
                            <button type="button" className="close" aria-label="Close" onClick={_ => this.setState({errorMessage: null})}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    )}

                    <div style={{overflowX: "auto"}}>
                        <table className={"table dataTable table-advanced-search dataTable-" + tList.id}>
                        <thead>
                            <tr role="row">
                                {isRowExpandable && (
                                  <th style={{width: "30px"}}/>
                                )}
                                {actionsHeader(ActionPosition.LEFT)}
                                {tList?.baseRow?.fields.map((iField) => {
                                    const field = iField as TField;
                                    const {sortingOrder, sortingFieldId} = tList;

                                    if (field.isVisible) {
                                        return (
                                            <th scope="col"
                                                key={field.id}
                                                className={("header_list_field_" + field.kind + " ").toLowerCase()
                                                  + ("header_list_field_" + field.type).toLowerCase() + " "
                                                  + (!field.isSortable ? "" : sortingFieldId === field.id ? (sortingOrder > 0 ? "sorting_asc" : "sorting_desc") : "sorting")}
                                                onClick={() => { // FIXME: extract in a method
                                                    if (!field.isSortable) {
                                                        return;
                                                    }

                                                    this.sortList(field);
                                                }}
                                            >
                                                <div className="sorting__container">
                                                    <div className="sorting__text">{t(field.label ?? "")}</div>
                                                </div>
                                            </th>
                                        );
                                    }

                                    return null;
                                })}

                                {/* header for the NEW badge column */}
                                <th scope="col"/>

                                {this.props.onSelectItem && (
                                    // Only display selection column if onSelectItem handler has been defined
                                    <th scope="col"/>
                                )}
                                {actionsHeader(ActionPosition.RIGHT)}
                            </tr>
                        </thead>
                        <tbody>
                            {pagedTList.map((row, iIndex) => {
                                // Generate a key from all primary key values of the row
                                const rowKey = row.getPrimaryKeyFields(tList.baseRow)
                                  .reduce((prev, cur, index) => (
                                    prev + ((index !== 0 ? "_" : "") + cur.value)
                                  ), "");
                                row.nr = iIndex;
                                let rowClassname = "";
                                let rowSupplClassName = "";

                                const rowClassNameField = StringUtils.getNString(tList.customObjectParams?.["ROW.CLASS_NAME_FIELD"]);
                                if (rowClassNameField !== "") {
                                    const rowClassNameFieldValue = row.getFieldById(rowClassNameField)?.stringValue;
                                    rowClassname = ("row_" + tList.id + "_" + rowClassNameField + "_" + rowClassNameFieldValue).toLowerCase();
                                }

                                if (this.props.onClickRow != null) {
                                    rowSupplClassName = " row-clickable";
                                }

                                return (
                                    <Fragment key={rowKey}>
                                        <tr role="row" className={rowClassname + rowSupplClassName}  onClick={() => {
                                            this.props.onClickRow?.(row);
                                        }}>
                                            {(isRowExpandable) && (
                                                // If the row has an __EXPANDABLE__ field with a Trunks truthy value ('Y', "true", true, etc.)
                                                // the row is expandable and display expand arrow,
                                                // otherwise, the row is not expandable (don't display expand arrow)
                                                (row.getFieldById("__EXPANDABLE__")?.booleanValue ?? true) ? (
                                                    <td style={{width: "30px"}} onClick={() => {
                                                        if (isRowExpandable) {
                                                            this.setState({
                                                                expandedRowIndexes: this.state.expandedRowIndexes.includes(rowKey) ?
                                                                  [...this.state.expandedRowIndexes.filter(i => i !== (rowKey))]
                                                                  : [...this.state.expandedRowIndexes, (rowKey)]
                                                            });
                                                        }
                                                    }}>
                                                        <div className="__expandable">
                                                            {this.state.expandedRowIndexes.includes(rowKey) ?
                                                              <div className="-expanded"><span/></div>
                                                              : <div className="-collapsed"><span/></div>}
                                                        </div>
                                                    </td>
                                                ) : (
                                                    <td style={{width: "30px"}}/>
                                                )
                                            )}
                                            {actionsBody(ActionPosition.LEFT, row)}
                                            {tList?.baseRow?.fields.map((iField) => {
                                                const baseField = iField as TField;
                                                if (baseField.isVisible) {
                                                    return (
                                                        <td key={baseField.id}
                                                            className={("list_field list_field_" + baseField.kind + " list_field_" + baseField.type + " " + tList.id + "-" + baseField.id).toLowerCase()}>
                                                            <ListField
                                                                baseField={baseField}
                                                                row={row}
                                                                tList={tList}
                                                                additionalCallbacks={{
                                                                    "onEditForm": (this.props as ITFormProps).onEditForm,
                                                                    //"handleDeleteItem": this.handleDeleteItem
                                                                    "expandRow": () => {
                                                                        this.setState({
                                                                            expandedRowIndexes: this.state.expandedRowIndexes.includes(rowKey) ?
                                                                              [...this.state.expandedRowIndexes.filter(i => i !== (rowKey))]
                                                                              : [...this.state.expandedRowIndexes, (rowKey)]
                                                                        })
                                                                    }
                                                                }}
                                                                onRowFieldAction={this.props.onRowFieldAction}
                                                                onRefresh={this.refreshList}
                                                                refetch={this.props.refetch}
                                                                refetchSubList={() => {
                                                                    if (this.sublistRefs[rowKey]) {
                                                                        const x = this.sublistRefs[rowKey] as AdvancedSearchHandler;
                                                                        x.refresh();
                                                                    }
                                                                }}
                                                                onChange={(field: TField, value: any) => {
                                                                    const newTList = TList.clone(tList);
                                                                    field.value = value;

                                                                    // // FIXME: Must be done before saving the list
                                                                    // if (row.getFieldById("SYNC_STATUS")) {
                                                                    //     row.getFieldById("SYNC_STATUS").value = TRowState.MODIFIED;
                                                                    // }

                                                                    this.props.setTList(newTList);
                                                                }}
                                                            />
                                                        </td>
                                                    );
                                                }

                                                return null;
                                            })}

                                            {/* display a badge if this is new row */}
                                            <td className="status-col">
                                                {(row.getPrimaryKeyValue(tList.baseRow) === "") && (
                                                    <span className="badge badge-new">{t("data_table.badge_new")}</span>
                                                )}
                                            </td>

                                            {this.props.onSelectItem && (
                                                // Only display selection button if onSelectItem handler has been defined
                                                <td>
                                                    <button className="button button-transparent button-np" onClick={(_) => this.handleClickItem(row) }>
                                                        <i className="fas fa-angle-double-right" />
                                                    </button>
                                                </td>
                                            )}
                                            {actionsBody(ActionPosition.RIGHT, row)}
                                        </tr>

                                        {this.state.expandedRowIndexes.includes(rowKey) && (
                                            <tr key={iIndex + "-detail"}>
                                                {/*Empty td for alignment*/}
                                                <td/>
                                                <td className="expanded-row" colSpan={tList.baseRow.fields.filter(f => (f as TField).isVisible).length + 1}>
                                                    {subLists.map(subListId => (
                                                        <AdvancedSearch
                                                            ref={r => (this.sublistRefs[rowKey] = r)}
                                                            key={subListId}
                                                            dataSourceId={subListId}
                                                            enableDelete={false}
                                                            searchAtFirstDisplay={true}
                                                            hidePagination={true}
                                                            tRowParams={new TRow([
                                                              ...row.getPrimaryKeyFields(tList.baseRow),
                                                                new TField("LANGUAGE", i18n.language)
                                                            ])}
                                                        />
                                                    ))}
                                                </td>
                                            </tr>
                                        )}
                                  </Fragment>
                                );
                            })}
                            {StringUtils.stringToBoolean(tList.customObjectParams?.SUM) && (
                                <tr style={{borderTop: "5px"}}>
                                    {tList?.baseRow?.fields.map((iField) => {
                                        const baseField = iField as TField;
                                        if (baseField.isVisible) {
                                            if (StringUtils.stringToBoolean(baseField.customObjectParams?.SUM)) {
                                                const reducer = (accumulator: number, currentValue: TRow) => accumulator + (parseFloat(currentValue.getFieldById(iField.id)!.stringValue) || 0.0);
                                                const sum = tList.rows.reduce(reducer, 0); // Note: use pagedTList instead of tList.rows to display the sum per page
                                                const precision = (baseField.customObjectParams?.PRECISION && parseInt(baseField.customObjectParams.PRECISION)) || 2;

                                                return (
                                                    <td key={baseField.id}
                                                        title={tList.rows.length > 1 ?
                                                          t("trunks:data_table.sum_title", {length: tList.rows.length})
                                                          : undefined
                                                        }
                                                        className={(("list_field list_field_sum list_field_" + baseField.kind) + (" list_field_" + baseField.type) + " " + (tList.id + "-" + baseField.id)).toLowerCase()}>
                                                        {baseField.type === "INTEGER" ?
                                                          NumberUtils.roundDecimal(sum, 0)
                                                            .toLocaleString(config.i18n.language)
                                                          : NumberUtils.roundDecimal(sum, precision)
                                                            .toLocaleString(config.i18n.language, {
                                                                minimumFractionDigits: precision,
                                                                maximumFractionDigits: precision
                                                            })
                                                        }
                                                        {baseField.customObjectParams?.DATA_SUFFIX && (" " + baseField.customObjectParams?.DATA_SUFFIX)}
                                                    </td>
                                                );
                                            }
                                            else {
                                                return <td key={baseField.id}/>;
                                            }
                                        }

                                        return null;
                                    })}
                                </tr>
                            )}
                        </tbody>
                    </table>
                    </div>
                    {tList.hasAddFooter && <AddRow dataTable={this} baseRow={tList.baseRow} onAddRow={(row: TRow) => {
                        // Add row in memory
                        const newTList = TList.clone(tList);
                        newTList.rows.push(row);
                        this.props.setTList(newTList);
                    }}/>}

                    {this.displayNumberOfRows()}

                    {!this.props.hidePagination && (
                        <div className="row">
                            <div className="col-sm-12 col-md-5">
                                <div className="dataTables_length dataTable-show-entries">
                                    <label>{t("data_table.label_show")} {this.rowsPerPageSelect()} {t("data_table.label_entries")}</label>
                                </div>
                            </div>
                            <div className="col-sm-12 col-md-7">
                                <div className="dataTables_paginate paging_simple_numbers">
                                    <ul className="pagination">
                                        {this.paginateButtons()}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    )}
                </div>

                {this.modalConfirmDelete()}
            </>
        );
    }
}

interface IAddRow {
    dataTable: DataTable,
    baseRow: TRow,
    onAddRow?: (row: TRow) => void,
}

/**
 *
 * @param dataTable
 * @param baseRow
 * @param onAddRow
 * @constructor
 */
function AddRow({dataTable, baseRow, onAddRow}: IAddRow) {
    const [row, setRow] = useState<TRow | null>(null);
    const [errors, setErrors] = useState<string[]>([]);

    const refreshRow = useCallback(() => {
        const newTRow = new TRow([], undefined, -5);

        baseRow.fields.forEach(baseField => {
            const newTRowField = Object.assign(new TField(baseField.id), baseField);
            newTRow.fields.push(newTRowField);
        });

        setRow(newTRow);
    }, [baseRow, setRow]);

    /**
     * handleAddRow
     */
    const handleAddRow = useCallback((jsEvent: any) => {
        jsEvent.preventDefault();

        const {t} = config;
        // Generic validation of the row
        const rowErrors = GenericValidationUtils.validateRow(row!, baseRow, true);
        const errors = Object.values(rowErrors).flat();
        setErrors(errors);

        if (errors.length > 0) {
            return;
        }

        if (StringUtils.stringToBoolean(dataTable.props.tList.customObjectParams?.INSERT_ROW_ON_ADD)) {
            // Insert the row on the server with an API call
            const tInsertRowDTO = new TInsertRowDTO({
                row: row!,
                params: dataTable.props.tRowParams
            })

            const dataWebService = new TrunksWebService();
            dataWebService.insertRow(async (_) => {
                AlertManager.showSuccessToast(t("data_table.msg_insert_success"));

                dataTable.setState({errorMessage: null});

                // If there is a callback props 'onDidInsertItem', the insert is the responsibility of this callback
                if (dataTable.props.onDidInsertItem) {
                    await dataTable.props.onDidInsertItem(row!);
                }
                else {
                    // TODO
                    // refresh data table with retrieveListAndData
                }

                refreshRow();

            }, (error: any) => {
                // Display error
                dataTable.setState({
                    errorMessage: TranslationUtils.getErrorTranslation(t, error.message)
                });
            }, dataTable.props.tList.id, tInsertRowDTO)
                .catch(error => {
                    Logger.e("<<<<<<< DataTable.handleAddRow.error...", error);
                    dataTable.setState(() => { throw error; });
                });
        }
        else {
            onAddRow?.(row!);
            refreshRow();
        }
    }, [row, baseRow, dataTable, refreshRow, onAddRow]);

    /**
     * useEffect
     */
    useEffect(() => {
        refreshRow();
    }, [baseRow, refreshRow]);

    if (!row) return null;

    return (
        <>
            <FormErrors formErrors={errors} />

            <table className={"table dataTable table-advanced-search"} style={{marginTop: "1rem"}}>
                <tbody>
                <tr style={{backgroundColor: "var(--color-primary01)"}}>
                    {row.fields.map((rowField) => (
                        (rowField as TField).isVisibleAdd && (
                            <td key={rowField.id}>
                                <ListField baseField={baseRow.getFieldById(rowField.id)!}
                                       row={row}
                                       onChange={(field: TField, value: any) => {
                                           field.value = value;
                                           setRow(new TRow(row.fields))
                                       }}
                                />
                            </td>
                        )
                    ))}
                    <td>
                        <button className="button button-transparent p-0" onClick={handleAddRow}>
                            <i className="fas fa-plus" />
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </>
    );
}

export default DataTable;
