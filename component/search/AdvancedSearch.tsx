import React, {FC, ForwardedRef, useCallback, useEffect, useImperativeHandle, useRef, useState} from "react";

import StringUtils from "../../../trunks-core-js/utilities/StringUtils";
import Logger from "../../../trunks-core-js/logger/logger";
import TList from "../../../trunks-core-js/model/TList";
import TRow from "../../../trunks-core-js/model/TRow";
import TField, {TFieldValue} from "../../../trunks-core-js/model/TField";
import TSimpleField from "../../../trunks-core-js/model/TSimpleField";

import TFilterAndSortListDTO from "../../../trunks-core-js/web-service/dto/TFilterAndSortListDTO";
import TSortListDTO from "../../../trunks-core-js/web-service/dto/TSortListDTO";
import TFilterListDTO from "../../../trunks-core-js/web-service/dto/TFilterListDTO";
import TRetrieveListAndDataDTO from "../../../trunks-core-js/web-service/dto/TRetrieveListAndDataDTO";
import TrunksWebService from "../../../trunks-core-js/web-service/TrunksWebService";
import ObjectUtils from "../../../trunks-core-js/utilities/ObjectUtils";

import DataTable, {ActionPosition, IDataTableRowProps} from "../datatable/DataTable";
import ITFormProps from "../modal/wrapper/ITFormProps";
import withModalTForm from "../modal/wrapper/withModalTForm";
import SearchField from "./field/SearchField";
import ErrorManager from "../../ui/ErrorManager";
import AlertManager from "../../ui/AlertManager";

//import './AdvancedSearch.scss';
import {config} from "../../config";
import LoadingWrapper, {Loader} from "../LoadingWrapper";
import TWebForm from "trunks-core-js/model/TWebForm";
import DropDown, {DropDownButton} from "../dropdown/DropDown";

type IAdvancedSearchProps = IProps | (IProps & ITFormProps);

interface IProps extends IDataTableRowProps {
    dataSourceId: string,
    tRowParams?: TRow,

    onCancel?: () => void, // Called when 'cancel' button of Advanced search is clicked. (Callback passed by the component calling AdvancedSearch)

    nbSearchColumn?: number,

    searchAtFirstDisplay: boolean,

    onError?: (error: any) => void,

    title?: string,

    isModal?: boolean,

    enableSave?: boolean,

    actionsPosition?: ActionPosition,

    header?: (tList: TList) => JSX.Element,

    // Used to override title of the 'New' button
    newFormButtonTitle?: string,
}

export interface AdvancedSearchHandler {
    refresh: () => void,
    getTList: () => TList | null
}

const AdvancedSearch: React.ForwardRefRenderFunction<AdvancedSearchHandler, IAdvancedSearchProps> = (props: IAdvancedSearchProps, forwardedRef) => {
    //const {t} = useTranslation("advancedSearch");
    const {t} = config;
    const {searchAtFirstDisplay = true, isModal = false} = props;

    const [tList, setTList] = useState<TList | null>(null);
    const [filters, setFilters] = useState<TRow>(new TRow());
    const [isSearchDone, setIsSearchDone] = useState(false);
    const [isFiltersOpen, setIsFiltersOpen] = useState(false);
    const [isFiltering, setIsFiltering] = useState(false);

    const [, setError] = useState(false);

    const dataTableRef = useRef<DataTable>(null);

    const tFormComponent = (props as ITFormProps).tFormComponent;

    // The DataTable HOC component computed based on the tFormComponent props
    let [DataTableWithModalTForm, setDataTableWithModalTForm] = useState<any>();

    // Provide to the parent component a method to refresh the datatable
    useImperativeHandle(forwardedRef, () => ({
        refresh: refresh,
        getTList: getTList
    }));

    // Reference that saves the previous props
    const prevProps = useRef<IAdvancedSearchProps>();

    useEffect(() => {
        Logger.d("AdvancedSearch.useEffect...");

        // If the dataSourceId or the tRowParams props didn't change since last useEffect run => do not re-fetch
        if (prevProps.current
            && prevProps.current.dataSourceId === props.dataSourceId
            && ObjectUtils.deepEquals(prevProps.current.tRowParams, props.tRowParams)
        ) {
            // Save prevProps in ref
            prevProps.current = props;
            return;
        }

        /**
         * Set filter default values
         * @param tList
         */
        const setDefaultFilters = (tList: TList) => {
            const defaultFilters = new TRow([]);
            tList.searchRow.fields.forEach((searchField) => {
                if (StringUtils.getNString(searchField.value) !== "" && !(searchField.value instanceof TRow)) {
                    defaultFilters.fields.push(new TSimpleField(searchField.id, searchField.value));
                }
            });

            setFilters(defaultFilters);
        }

        const dataWebService = new TrunksWebService();

        if (searchAtFirstDisplay) { // Execute a search at the first display if required
            dataWebService.retrieveListAndData((tList: TList) => {
                setTList(tList);
                setDefaultFilters(tList);
                setIsFiltersOpen(StringUtils.stringToBoolean(tList?.customObjectParams?.DEFAULT_FILTERS_OPEN ?? "true"));
                setIsSearchDone(true);
            }, (error) => {
                // Handle error
                props.onError?.(error);
            }, props.dataSourceId, new TRetrieveListAndDataDTO({params: props.tRowParams}))
            .catch(error => {
                Logger.e("<<<<<<< AdvancedSearch.useEffect.error...", error);
                setError(() => { throw error; });
            });
        }
        else {
            dataWebService.getListObject((tList: TList) => {
                setTList(tList);
                setDefaultFilters(tList);
                setIsFiltersOpen(StringUtils.stringToBoolean(tList?.customObjectParams?.DEFAULT_FILTERS_OPEN ?? "true"));
            }
            , (error) => {
                Logger.e("AdvancedSearch.useEffect - getListObject(" + props.dataSourceId + ")... ", error);
                ErrorManager.displayErrorMessage(error);
            }
            , props.dataSourceId)
            .catch(error => {
                Logger.e("<<<<<<< AdvancedSearch.useEffect.error...", error);
                setError(() => { throw error; });
            });
        }

        // Save prevProps in ref
        prevProps.current = props;
    }, [searchAtFirstDisplay, t, props]);


    // Lifecycle method called before updating the Component.
    // Used to recompute the DataTable HOC if needed.
    useEffect(() => {
        if (tFormComponent) {
            setDataTableWithModalTForm(withModalTForm(DataTable, tFormComponent));
        }
    }, [tFormComponent]);

    const refresh = async () => {
        if (isSearchDone) {
            await handleSearch();
        }
    }

    /**
     * getTList
     */
    const getTList = () => {
        return tList;
    }

    /**
     * getTWebForm
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const getTWebForm = () => {
        return tList;
    }

    /**
     * setTWebForm
     * This method is given to SearchFied -> FieldSelect and allows to refresh TList from a custom on change action
     * @param tWebForm
     */
    const setTWebForm = (tWebForm: TWebForm) => {
        setTList(tWebForm as TList);
    }

    /**
     * handleCancel
     * @param event
     */
    const handleCancel = (event: any) => {
        event?.preventDefault();
        props.onCancel?.();
    }

    /**
     * Returns filter values and params
     */
    const getTFilterListDTO = () : TFilterListDTO => {
        return new TFilterListDTO({
            filters: filters,
            params: props.tRowParams
        });
    }

    /**
     * handleSearch
     * @param event
     */
    const handleSearch = async (event?: any) => {
        Logger.d("AdvancedSearch.handleSearch...");
        event?.preventDefault();

        if (tList == null) {
            return;
        }

        setIsFiltering(true);

        const tFilterListDto = getTFilterListDTO();
        // Logger.d("AdvancedSearch.handleSearch - tFilterListDto", tFilterListDto);
        // Logger.d("AdvancedSearch.handleSearch - tList.sortingFieldId = " + tList.sortingFieldId + " - sortingOrder = " + tList.sortingOrder);

        const dataWebService = new TrunksWebService();
        if (tList.isDBSort) {
            // ---------------------------------------------------------------------------------------------------------
            // ----- CALL WEB SERVICE filterAndSortList
            // ---------------------------------------------------------------------------------------------------------
            let tSortListDTO = null;
            if (StringUtils.getNString(tList.sortingFieldId) !== "") {
                tSortListDTO = new TSortListDTO({sortingFieldId : tList.sortingFieldId!, sortingOrder: tList.sortingOrder})
            }

            const tFilterAndSortListDto = new TFilterAndSortListDTO(
                {
                    tFilterListDTO: tFilterListDto,
                    tSortListDTO : tSortListDTO ?? undefined
                });

            // call web service
            await dataWebService.filterAndSortList((tListFilteredAndSorted: TList) => {
                    if (StringUtils.stringToBoolean(tListFilteredAndSorted.customObjectParams?.ONLY_SEND_ROWS_ON_FILTER ?? "true")) {
                       // filterAndSortList - ONLY_SEND_ROWS_ON_FILTER is true => keep the current base row

                        const clonedTList = TList.clone(tList);
                        clonedTList.rows = tListFilteredAndSorted.rows.map((row: any) => TRow.fromJson(row));
                        // Reset the nbRowsToRetrieve properties according to new retrieved rows
                        clonedTList.isMaxRowToRetrieveReached = tListFilteredAndSorted.isMaxRowToRetrieveReached;
                        clonedTList.maxRowToRetrieve = tListFilteredAndSorted.maxRowToRetrieve;
                        clonedTList.limitMaxRowToRetrieve = tListFilteredAndSorted.limitMaxRowToRetrieve;
                        setTList(clonedTList);
                    }
                    else {
                        // filterAndSortList - ONLY_SEND_ROWS_ON_FILTER is false => replace current tList by the new one
                        // Re-sort the new TList if the old one was sorted
                        if (tList.sortingFieldId != null) {
                            tListFilteredAndSorted.sort(tListFilteredAndSorted.baseRow.getFieldById(tList.sortingFieldId)!, tList.sortingOrder);
                        }
                        setTList(tListFilteredAndSorted);
                    }

                    dataTableRef.current?.goToFirstPage();
                    setIsSearchDone(true);
            }
            , (error) => {
                Logger.e("DataTable.sortList - filterAndSortList(" + tList.id + ")... ", error);
                ErrorManager.displayErrorMessage(error);
            }
            , tList.id, tFilterAndSortListDto)
            .catch(error => {
                setError(() => { throw error; });
            }).finally(() => setIsFiltering(false));
        }
        else {
            // ---------------------------------------------------------------------------------------------------------
            // ----- CALL WEB SERVICE filterList
            // ---------------------------------------------------------------------------------------------------------
            await dataWebService.filterList((tListFilteredAndSorted: TList) => {
                    if (StringUtils.stringToBoolean(tListFilteredAndSorted.customObjectParams?.ONLY_SEND_ROWS_ON_FILTER ?? "true")) {
                        // filterList - ONLY_SEND_ROWS_ON_FILTER is true => clone current tList and just replace rows
                        const clonedTList = TList.clone(tList);
                        clonedTList.rows = tListFilteredAndSorted.rows.map((row: any) => TRow.fromJson(row));
                        // Reset the nbRowsToRetrieve properties according to new retrieved rows
                        clonedTList.isMaxRowToRetrieveReached = tListFilteredAndSorted.isMaxRowToRetrieveReached;
                        clonedTList.maxRowToRetrieve = tListFilteredAndSorted.maxRowToRetrieve;
                        clonedTList.limitMaxRowToRetrieve = tListFilteredAndSorted.limitMaxRowToRetrieve;
                        setTList(clonedTList);
                    }
                    else {
                        // filterList - ONLY_SEND_ROWS_ON_FILTER is false => replace current tList by the new one
                        // Re-sort the new TList if the old one was sorted
                        if (tList.sortingFieldId != null) {
                            tListFilteredAndSorted.sort(tListFilteredAndSorted.baseRow.getFieldById(tList.sortingFieldId)!, tList.sortingOrder);
                        }
                        setTList(tListFilteredAndSorted);
                    }

                    dataTableRef.current?.goToFirstPage();
                    setIsSearchDone(true);
            }
            , (error) => {
                Logger.e("AdvancedSearch.handleSearch - getFilteredListObject(" + props.dataSourceId + ")... ", error);
                ErrorManager.displayErrorMessage(error);
            }
            , props.dataSourceId, tFilterListDto)
            .catch(error => {
                setError(() => { throw error; });
            })
            .finally(() => setIsFiltering(false));
        }
    }

    const handleChangeFilter = (field: TField, value: TFieldValue | null) => {
        const filterId = field.id;

        let filtersCopy = Object.assign(new TRow(), filters);
        const index = filtersCopy.fields.findIndex(item => item.id === filterId);

        // New filter
        if (index === -1) {
            if (value !== "" && value != null) {
                filtersCopy.fields.push(new TSimpleField(filterId, value));
            }
        }
        // Existing filter
        else {
            // Update filter value
            if (value !== "" && value != null) {
                filtersCopy.fields[index].value = value;
            }
            // Remove the filter from the array of filters
            else {
                filtersCopy.fields = filtersCopy.fields.filter(item => item.id !== filterId);
            }
        }

        field.value = value;
        setFilters(filtersCopy);
    }

    const handleDidInsertRow = async (tRow: TRow) => {
        Logger.d("AdvancedSearch.handleDidInsertRow...");
        await refresh();
    }

    const handleDidDeleteRow = async (tRow: TRow, nbDeletedRows: number) => {
        Logger.d("AdvancedSearch.handleDidDeleteRow...");
        await refresh();
    }

    /**
     * dataTable
     * @private
     */
    const dataTable = () => {
        if (tList === null) {
            return null;
        }
        else if (tList?.rows && isSearchDone) {
            // FIXME: find a way to get props from IModalTFormProps using another method
            const modalTFormProps = props as ITFormProps;

            //Logger.d("AdvancedSearch.dataTable() - tFormDataSourceId = " + modalTFormProps.tFormDataSourceId);

            if (StringUtils.getNString(modalTFormProps.tFormDataSourceId) !== "") {
                return (
                    <DataTableWithModalTForm
                        ref={dataTableRef}
                        // ModalTForm props
                        tFormDataSourceId={modalTFormProps.tFormDataSourceId}
                        // onCreateFromForm={this.props.onCreateFromForm}
                        // onUpdateFromForm={this.state.onUpdateFromForm}

                        tList={tList}
                        tRowParams={props.tRowParams}
                        setTList={setTList}
                        getTFilterListDTO={getTFilterListDTO}
                        onSelectItem={props.onSelectItem}
                        onClickRow={props.onClickRow}
                        onRowFieldAction={props.onRowFieldAction}
                        actionsPosition={ActionPosition.LEFT}

                        onDidInsertItem={handleDidInsertRow}
                        onDidDeleteItem={handleDidDeleteRow}

                        refetch={handleSearch}

                        // Put props at the end to keep the possibility to override them (example: actionsPosition)
                        {...props}
                    />
                );
            }
            else {
                return (
                    <DataTable
                        ref={dataTableRef}
                        tList={tList}
                        tRowParams={props.tRowParams}
                        setTList={setTList}
                        getTFilterListDTO={getTFilterListDTO}
                        onSelectItem={props.onSelectItem}
                        onClickRow={props.onClickRow}
                        onRowFieldAction={props.onRowFieldAction}
                        actionsPosition={ActionPosition.LEFT}
                        //enableDelete={props.enableDelete}
                        onDidInsertItem={handleDidInsertRow}
                        onDidDeleteItem={handleDidDeleteRow}
                        refetch={handleSearch}
                        {...props}
                    />
                );
            }
        }

        return null;
    }

    /**
     * displayHeader
     * @private
     */
    const displayHeader = () => {
        const title = props.title ?? StringUtils.getNString(tList?.customObjectParams?.TITLE);
        const contextField = StringUtils.getNString(tList?.customObjectParams?.CONTEXT_FIELD);
        const onNewForm = modalTFormProps.onNewForm;

        if (!title && !contextField && !onNewForm) {
            return null;
        }

        return (
          <div className="row mt-l">
              <div className="col-8 col-lg-6">
                  {title && <h1 className="__title text-left">{t(title)}</h1>}
                  {contextField && <h2>{props.tRowParams?.getFieldById(contextField)?.stringValue}</h2>}
              </div>
              {modalTFormProps.onNewForm && (
                // Display "New" button only if the onNewForm callback has been defined
                <div className="col-4 col-lg-6 d-flex align-items-center justify-content-end">
                    <button className="button button-primary button-auto" onClick={modalTFormProps.onNewForm}>
                        {props.newFormButtonTitle || t("default.btn_new")}
                    </button>
                </div>
              )}
              {props.enableSave && (
                // Display "New" button only if the onNewForm callback has been defined
                <div className="col-4 col-lg-6 d-flex align-items-center justify-content-end">
                    <button className="button button-primary button-auto" onClick={(e) => dataTableRef.current?.saveList(e)}>
                        {t("default.btn_save")}
                    </button>
                </div>
              )}
          </div>
        );
    }



    /**
     * -----------------------------------------------------------------------------------------------------------------
     * render
     * -----------------------------------------------------------------------------------------------------------------
     */

    // FIXME: find a way to get props from IModalTFormProps using another method
    const modalTFormProps = props as ITFormProps;

    return (
        <>
            {tList && props.header?.(tList)}

            {isModal && (
                <div className="modal-header flex flex-middle">
                    {props.title && <h2>{props.title}</h2>}
                    {modalTFormProps.onNewForm && (
                        // Display "New" button only if the onNewForm callback has been defined
                        <button className="button button-primary button-modal" onClick={modalTFormProps.onNewForm}>
                            {props.newFormButtonTitle || t("default.btn_new")}
                        </button>
                    )}
                </div>
            )}

            {isModal && (
                <div className="modal-container">
                    <Filters
                        tList={tList}
                        nbSearchColumn={props.nbSearchColumn}
                        onChange={handleChangeFilter}
                        onSearch={handleSearch}
                        isFiltersOpen={isFiltersOpen}
                        setIsFiltersOpen={setIsFiltersOpen}
                        setTWebForm={setTWebForm}
                    />

                    <LoadingWrapper isLoading={tList == null || isFiltering}>
                        {dataTable()}
                    </LoadingWrapper>
                </div>
            )}

            {!isModal && (
                <>
                    {displayHeader()}

                    <Filters
                        tList={tList}
                        setTWebForm={setTWebForm}
                        nbSearchColumn={props.nbSearchColumn}
                        onChange={handleChangeFilter}
                        onSearch={handleSearch}
                        isFiltersOpen={isFiltersOpen}
                        setIsFiltersOpen={setIsFiltersOpen}
                    />

                    <LoadingWrapper isLoading={tList == null || isFiltering}>
                        {dataTable()}
                    </LoadingWrapper>
                </>
            )}

            {!isFiltering && tList && (
              <ExcelImportExport
                tList={tList}
                dataSourceId={props.dataSourceId}
                tRowParams={props.tRowParams}
                filters={filters}
              />
            )}

            {props.onCancel && (
                <div className="modal-footer block">
                    <button className="button button-transparent button-auto button-np my-s" onClick={handleCancel}>
                        {t("default.btn_cancel")}
                    </button>
                </div>
            )}
        </>
    );
}

interface IFilterProps {
    tList: TList | null,
    onChange: (field: TField, value: TFieldValue | null) => void,
    onSearch: () => void,
    nbSearchColumn?: number,
    isFiltersOpen: boolean,
    setIsFiltersOpen: (boolean: boolean) => void,
    setTWebForm: (tWebForm: TWebForm) => void
}

const Filters = (props: IFilterProps): any => {
    //const {t} = useTranslation("advancedSearch");
    const {t} = config;
    const {tList, onChange, onSearch, isFiltersOpen, setIsFiltersOpen, setTWebForm} = props;

    if (tList == null) {
        return null;
    }

    // The list does not have any filters => don't display the panel
    if (tList.searchRow?.fields.length === 0) {
        return null;
    }

    return(
        <form className="advancedsearch" style={{paddingBottom: (isFiltersOpen ? "0" : "var(--grid-s)")}}>
            <div className="row">
                <div className="col">
                   <h3 onClick={_ => { setIsFiltersOpen(!isFiltersOpen);}}
                       style={{textAlign: "left", cursor: "pointer"}}><i className={isFiltersOpen ? "fas fa-angle-down" : "fas fa-angle-right"}/>&nbsp;{t("advanced_search.label_filters")}</h3>
                </div>
            </div>
            <div className="row" style={{display: (isFiltersOpen ? "flex" : "none")}}>
                {tList.searchRow?.fields.map((iField) => {
                    const field = iField as TField;
                    return (
                        <SearchField containerClassName={"col-12 col-md-6 col-lg-4 col-xl-" + (12 / tList.nbColCriteria) + " mb-xs"}
                                     key={field.id}
                                     baseField={field}
                                     field={field}
                                     onChange={onChange}
                                     blankOptionLabel={field.customObjectParams?.BLANK_OPTION_LABEL}
                                     tList={tList}
                                     setTWebForm={setTWebForm}
                        />
                    );
                })}
                <div className="col flex flex-bottom pb-s">
                    <button className="button button-secondary button-auto button-p-xs button-xs" onClick={onSearch}>
                        {t("advanced_search.btn_apply")}
                    </button>
                </div>
            </div>
        </form>
    );
}


/**
 *
 * @param props
 * @constructor
 */
function ExcelImportListFile({idList, params}: {idList: string, params?: TRow}) {
    const {t} = config;
    const fileInputRef: React.LegacyRef<HTMLInputElement> = useRef(null);
    const [file, setFile] = useState<File | null>(null);

    const handleChangeFile = useCallback((jsEvent: React.ChangeEvent<HTMLInputElement>) => {
        setFile(jsEvent.target.files?.[0] ?? null);
    }, [setFile]);

    const resetFileInput = () => {
        if (fileInputRef.current) {
            fileInputRef.current.files = null;
        }

        setFile(null);
    }

    const sendFile = async (jsEvent: React.FormEvent<HTMLFormElement>) => {
        jsEvent.preventDefault();

        if (file == null) {
            AlertManager.showWarningToast(t("excel_import_file.error.file_required"));
            return;
        }

        try {
            const dataWebService = new TrunksWebService();
            await dataWebService.importXls(() => {
                AlertManager.showSuccessToast(t("excel_import_file.import_success"));
                resetFileInput();
            }, (jsonResponse) => {
                const errors = (
                    <ul>
                        {jsonResponse.data.map((error: any) => <li key={error}>{error}</li>)}
                    </ul>
                );
                AlertManager.showErrorToast(errors);
                resetFileInput();
            }, idList, file, params);
        }
        catch(e) {
            AlertManager.showErrorToast(t("excel_import_file.error.import_failed"));
            resetFileInput();
        }
    }

    return (
        <form className="ml-s" onSubmit={sendFile}>
            <button className="button button-auto mt-s ml-s button-import-xls"
                    type="submit"
                    disabled={file == null}
            >
                {t("excel_import_file.import")}
            </button>
            <input type="file"
                   accept=".xls"
                   ref={fileInputRef}
                   className={"ml-s"}
                   onChange={handleChangeFile}
            />

        </form>
    );
}

interface IExcelExportListFile {
    tList: TList;
    dataSourceId: string
    tRowParams?: TRow
    filters?: TRow;
}

const ExcelExportListFile: FC<IExcelExportListFile> = ({tList, filters, dataSourceId, tRowParams}) => {
    const {t} = config;
    const [exporting, setExporting] = useState(false);

    /**
     * Call API to export the list into an Excel (XSL) file
     */
    const handleExportXls = async (ignoreFilters: boolean) => {
        const tFilterAndSortListDTO = new TFilterAndSortListDTO({
            tFilterListDTO: new TFilterListDTO({
                  filters: ignoreFilters ? undefined : filters,
                  params: tRowParams
              }),
            tSortListDTO: undefined // TODO: give sort dto if list.isDbSort
        });

        const dataWebService = new TrunksWebService();
        return dataWebService.exportXls(null,
          () => {
              AlertManager.showErrorToast(t("advanced_search.export_excel_failed"));
          },
          StringUtils.getEString(tList?.customObjectParams?.EXPORT_XLS_LIST, dataSourceId),
          tFilterAndSortListDTO);
    }

    return (
      <DropDown
        className="mt-s ml-s"
        buttonClassName="button-export-xls"
        content={exporting ? (
          <>
              <span className="xls-button-loader">
                  <Loader size="32"/>
              </span>
              {t("advanced_search.btn_exporting_xls")}
          </>
        ) : (
          t("advanced_search.btn_export_xls")
        )} disabled={exporting}>
          <DropDownButton onClick={async () => {
              try {
                  setExporting(true);
                  await handleExportXls(false);
              }
              finally {
                  setExporting(false);
              }
          }}>
              {t("advanced_search.export_xls_filtered")}
          </DropDownButton>

          <DropDownButton onClick={async () => {
              try {
                  setExporting(true);
                  await handleExportXls(true);
              }
              finally {
                  setExporting(false);
              }
          }}>
              {t("advanced_search.export_xls_full")}
          </DropDownButton>
      </DropDown>
    );
}

/**
 * renderExcelImportExport
 */
const ExcelImportExport: FC<IExcelExportListFile> = (props) => {
    if (!props.tList) return null;

    const exportExcel = StringUtils.stringToBoolean(props.tList.customObjectParams!.EXPORT_XLS);
    const importExcel = StringUtils.stringToBoolean(props.tList.customObjectParams!.IMPORT_XLS);

    if (!exportExcel && !importExcel) return null;

    return (
      <div className="row">
          {exportExcel && (
            <ExcelExportListFile {...props}/>
          )}
          {importExcel && (
            <ExcelImportListFile idList={props.dataSourceId} params={props.tRowParams}/>
          )}

      </div>
    );
}

const AdvancedSearchComponent = React.forwardRef(AdvancedSearch);


/**
 * ModalAdvancedSearch
 *
 * WARNING: this component doesn't include a Modal.
 * To display in a modal, it must be wrapped in a <Modal/> component.
 * It only returns an <AdvancedSearch/> with the modal props set to true
 */
export const ModalAdvancedSearch = React.forwardRef((props: Omit<IAdvancedSearchProps, "isModal">, forwardedRef: ForwardedRef<AdvancedSearchHandler>) => {
    return <AdvancedSearchComponent {...props} ref={forwardedRef} isModal={true}/>
});

export default AdvancedSearchComponent;
