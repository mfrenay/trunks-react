import TField, {DisplayType, TFieldValue} from "../../../../trunks-core-js/model/TField";
import TOptionValue from "../../../../trunks-core-js/model/TOptionValue";

import FieldInput from "../../form/field/FieldInput";
import FieldLabel from "../../form/field/FieldLabel";
import FieldSelect from "../../form/field/FieldSelect";
import {config} from "../../../config";
import TFieldKind from "trunks-core-js/model/TFieldKind";
import TList from "trunks-core-js/model/TList";
import TWebForm from "trunks-core-js/model/TWebForm";
import FieldSelectAutoComplete from "../../form/field/FieldSelectAutoComplete";

interface IProps {
    baseField: TField,
    field: TField,
    onChange: (field: TField, value: TFieldValue | null) => void,
    containerClassName: string,
    blankOptionLabel?: string,

    tList: TList,
    setTWebForm: (tWebForm: TWebForm) => void
}

function SearchField(props: IProps) {
    const field: TField = props.field;
    const {t} = config;
    const {containerClassName, blankOptionLabel, tList, setTWebForm, ...otherProps} = props;
    const baseField = props.baseField;

    switch (baseField.customDisplay) {
        case DisplayType.SELECT_AUTO_COMPLETE:
            return (
              <div className={props.containerClassName}>
                  <div className="textfield -s">
                      <label htmlFor={field.id}>{t(field.label ?? "")}</label>
                      <FieldSelectAutoComplete
                        tWebForm={tList}
                        setTWebForm={setTWebForm}
                        blankOptionLabel={blankOptionLabel} {...otherProps} />
                  </div>
              </div>
            );
    }

    // Return the right field according to the field's kind
    switch (field.kind) {
        case null:
        case TFieldKind.NONE:
        case TFieldKind.TEXTFIELD:
            return (
                <div className={props.containerClassName}>
                    <div className="textfield -s">
                        <label htmlFor={field.id}>{t(field.label ?? "")}</label>
                        <FieldInput {...otherProps} />
                    </div>
                </div>
            );
        case TFieldKind.CHECKBOX:
            return (
                <div className={props.containerClassName}>
                    <div className="textfield -s">
                        <label htmlFor={field.id}>{t(field.label ?? "")}</label>
                        <FieldSelect
                            tWebForm={tList}
                            setTWebForm={setTWebForm}
                            {...otherProps}
                            customOptions={[
                                new TOptionValue("Y", t("search_field.label_yes")),
                                new TOptionValue("N", t("search_field.label_no")),
                            ]}
                        />
                    </div>
                </div>
            );
        case TFieldKind.LABEL:
            return (
                <div className={props.containerClassName}>
                    <div className="textfield -s">
                        <label htmlFor={field.id}>{t(field.label ?? "")}</label>
                        <FieldLabel {...otherProps} />
                    </div>
                </div>
            );
        case TFieldKind.LIST_VAL:
        case TFieldKind.SELECT:
            return (
                <div className={props.containerClassName}>
                    <div className="textfield -s">
                        <label htmlFor={field.id}>{t(field.label ?? "")}</label>
                        <FieldSelect
                            tWebForm={tList}
                            setTWebForm={setTWebForm}
                            blankOptionLabel={blankOptionLabel} {...otherProps} />
                    </div>
                </div>
            );
        default:
            return null;
    }
}

export default SearchField;
