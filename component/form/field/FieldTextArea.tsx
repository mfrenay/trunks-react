import React from "react";

import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import {config} from "../../../config";

interface IProps {
    id?: string;
    name?: string;
    placeholder?: string;
    value?: string | string[] | number;

    baseField: TField,
    field: TField,
    onChange?: (field: TField, value: TFieldValue | null) => void
}

function FieldTextArea(props: IProps): JSX.Element {
    const { field, baseField, ...otherProps } = props;
    const { t } = config;

    return (
        <textarea
            id={props.id ?? baseField.id}
            name={props.name ?? baseField.id}
            className="form-control"
            rows={3}
            {...otherProps}
            placeholder={props.placeholder ?? t(baseField.label ?? "")}
            value={props.value ?? field.stringValue}
            onChange={jsEvent => {
                props.onChange?.(field, jsEvent.target.value);
            }}
        />
    );
}

export default FieldTextArea;
