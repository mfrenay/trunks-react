import TField from "../../../../trunks-core-js/model/TField";
import {config} from "../../../config";
import StringUtils from "../../../../trunks-core-js/utilities/StringUtils";

function FieldLabel(props: any) {
    const field: TField = props.field;
    const baseField: TField = props.baseField;
    const href = props.href;
    const {t} = config;

    const linkAction = StringUtils.getNString(baseField.customObjectParams?.LINK_ACTION);
    const fieldFormattedValue = (
      <>
          {field.getFormattedValue(baseField, t, {language: config.i18n.language})}
          {baseField.customObjectParams?.DATA_SUFFIX && field.value && (" " + baseField.customObjectParams?.DATA_SUFFIX)}
      </>
    );
    let content;

    if (linkAction === "#EMAIL#") {
        content = (
          <a href={"mailto:" + field.stringValue}>
              {fieldFormattedValue}
          </a>
        );
    } else if (linkAction === "#TEL#") {
        content = (
          <a href={"tel:" + field.stringValue}>
              {fieldFormattedValue}
          </a>
        );
    } else if (href) {
        content = (
          <a href={href}>
              {fieldFormattedValue}
          </a>
        );
    } else {
        content = fieldFormattedValue;
    }

    return (
      <div id={props.id ?? field.id} className={"field-label"}>
          {content}
      </div>
    );
}

export default FieldLabel;
