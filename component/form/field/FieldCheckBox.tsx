import React from "react";
import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";

interface IProps {
  field: TField;
  baseField: TField;
  className?: string;
  fieldErrors?: string[];
  id?: string;
  name?: string;
  placeholder?: string;
  onChange?: (field: TField, value: TFieldValue | null) => void;
}

function FieldCheckBox(props: IProps) {
    const {field} = props;

    return (
        <>
            <input type="checkbox"
                id={props.id ?? field.id}
                name={props.name ?? props.id ?? field.id}
                className={props.className}
                checked={field.booleanValue}
                onChange={jsEvent => {
                    props.onChange?.(field, jsEvent.target.checked);
                }}
                disabled={props.baseField.isDisabled}
            />
            <div className="checkbox__check"/>
        </>
    );
}

export default FieldCheckBox;
