import React from "react";

import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import {config} from "../../../config";

interface IProps {
    id?: string;
    name?: string;
    placeholder?: string;
    value?: string | string[] | number;

    // TForm props
    baseField: TField,
    field: TField,
    onChange?: (field: TField, value: TFieldValue | null) => void

    // If true, the value of the field is the unchecked options instead of the checked ones.
    // Example: considering the following options:
    // [ ] opt1
    // [X] opt2
    // [ ] opt3
    //   => field value when invertCheck=false => "opt2"
    //   => field value when invertCheck=true  => "opt1,opt3"
    invertCheck: boolean;
}

function FieldMultiCheckBox(props: IProps) {
    const {field, baseField, onChange} = props;
    const invertCheck: boolean = props.invertCheck ?? false;
    const {t} = config;

    const checkedOptions = field.stringValue.split(",")
        .filter(nb => nb !== "");

    return (
        <>
            {baseField.optionValues != null && baseField.optionValues.map((option, index) => (
                <div className="checkbox" key={option.value}>
                    <input type="checkbox"
                           value={option.value}
                           id={`multi_checkBox_${index}`}
                           checked={invertCheck ? !checkedOptions.includes(option.value) : checkedOptions.includes(option.value)}
                           onChange={jsEvent => {
                               if (onChange) {
                                   const fieldValue: string = jsEvent.target.value;
                                   let updatedOptions: string[];
                                   const mustAdd = (jsEvent.target.checked && !invertCheck) || (!jsEvent.target.checked && invertCheck);

                                   if (mustAdd) {
                                       updatedOptions = [...checkedOptions, fieldValue];
                                   } else {
                                       updatedOptions = checkedOptions.filter(val => val !== fieldValue);
                                   }

                                   onChange(field, updatedOptions.join(","));
                               }
                           }}
                    />
                    <div className="checkbox__check"/>
                    <label className="checkbox__label"
                           htmlFor={`multi_checkBox_${index}`}
                    >
                        {t(option.label)}
                    </label>
                </div>
            ))}
        </>
    );
}

export default FieldMultiCheckBox;
