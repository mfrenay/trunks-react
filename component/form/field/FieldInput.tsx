import React, {useEffect, useRef} from "react";
import DatePicker, {registerLocale} from "react-datepicker";
import fr from "date-fns/locale/fr";

import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import DateUtils from "../../../../trunks-core-js/utilities/DateUtils";
import ObjectUtils from "../../../../trunks-core-js/utilities/ObjectUtils";
import {config} from "../../../config";

import "react-datepicker/dist/react-datepicker.min.css";

registerLocale('fr', fr);

const DATE_FORMAT = "dd/MM/yyyy";
const DATETIME_FORMAT = "dd/MM/yyyy HH:mm";

interface IProps {
    id?: string;
    disabled?: boolean;
    name?: string;
    placeholder?: string;
    type?: string;
    value?: string | string[] | number;
    format?: string;
    focusField?: string;

    baseField: TField;
    field: TField;
    onChange?: (field: TField, value: TFieldValue | null) => void;

    timeIntervals?: number;
    yearDropdownItemNumber?: number;

    fieldErrors?: string[]
}

function FieldInput(props: IProps) {
    const {t, i18n} = config;
    const {focusField, field, baseField, fieldErrors, ...otherProps} = props;

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (props.focusField === baseField.id) {
            inputRef?.current?.focus();
        }
    }, [props.focusField, baseField]);

    /**
     * inputTypeForSimpleFieldType
     * @param simpleFieldType
     */
    function inputTypeForSimpleFieldType(simpleFieldType: string | null) {
        const defaultValue = "text";

        if (simpleFieldType == null) {
            return defaultValue;
        }

        switch (simpleFieldType) {
            case "VARCHAR":
                return "text";
            case "INTEGER":
                return "number";
            case "NUMBER":
                return "number";
            default:
                return defaultValue;
        }
    }

    const renderFieldErrors = () => {
        return (
            fieldErrors?.map((fieldError) => (
                <div className="invalid-feedback" style={{display: "block"}} key={fieldError}>
                    {fieldError}
                </div>
            ))
        );
    }

    if (baseField.type === "DATE" && !baseField.hasStartWildcard) {
        // Filter props to give to DatePicker
        const otherPropsForDatePicker = ObjectUtils.filterObject(otherProps, (key: any) => ["minTime", "maxTime", "minDate", "maxDate"].includes(key));

        return (
            <>
                <DatePicker
                    locale={i18n.language}
                    className={"form-control " + ((fieldErrors && fieldErrors?.length > 0) ? "is-invalid" : "")}
                    selected={field.stringValue === "" ? null : new Date(field.stringValue)}
                    showYearDropdown
                    yearDropdownItemNumber={props.yearDropdownItemNumber ?? 30}
                    scrollableYearDropdown
                    showTimeSelect={props.format === DATETIME_FORMAT}
                    dateFormat={props.format ?? DATE_FORMAT}
                    timeIntervals={props.timeIntervals}
                    timeCaption={t("field_input.label_date_picker_time")}
                    placeholderText={props.placeholder ?? t(baseField.label ?? "")}

                    {...otherPropsForDatePicker}

                    onChange={(date: Date) => {
                        props.onChange?.(field, date === null ? null : (props.format === DATETIME_FORMAT ? date.toISOString() : DateUtils.getFormattedDateYYYYMMDD(date)));
                    }}
                />
                {renderFieldErrors()}
            </>
        );
    } else {
        return (
            <>
                <input type={inputTypeForSimpleFieldType(baseField.type)}
                       min={baseField.customObjectParams?.MIN}
                       max={baseField.customObjectParams?.MAX}
                       step={baseField.customObjectParams?.STEP}
                       id={props.id ?? baseField.id}
                       name={props.name ?? baseField.id}
                       className={"form-control " + ((fieldErrors && fieldErrors?.length > 0) ? "is-invalid" : "")}
                       {...otherProps}
                       placeholder={props.placeholder ?? t(baseField.label ?? "")}
                       value={props.value ?? field.stringValue}
                       onChange={jsEvent => {
                           props.onChange?.(field, jsEvent.target.value);
                       }}
                       ref={inputRef}
                    /* IMPORTANT : maxLength is not working for input type number */
                />
                {renderFieldErrors()}
            </>
        );
    }
}

export default FieldInput;
export {DATE_FORMAT, DATETIME_FORMAT};
