import React from "react";

import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import TOptionValue from "../../../../trunks-core-js/model/TOptionValue";
import TypeUtils from "../../../../trunks-core-js/utilities/TypeUtils";
import {config} from "../../../config";
import StringUtils from "trunks-core-js/utilities/StringUtils";
import TWebForm from "trunks-core-js/model/TWebForm";
import {translate} from "trunks-core-js/utilities/TranslationUtils";

interface IProps {
    id?: string;
    name?: string;
    placeholder?: string;
    value?: string | string[] | number;

    // Label displayed for the option with blank value ("")
    blankOptionLabel?: string;

    baseField: TField,
    field: TField,
    isBooleanOptions?: boolean,
    customOptions?: TOptionValue[],
    onChange?: (field: TField, value: TFieldValue | null) => void,

    fieldErrors?: string[],

    tWebForm?: TWebForm,
    setTWebForm?: (tWebForm: TWebForm) => void
}

function FieldSelect(props: IProps) {
    const field: TField = props.field;
    const baseField: TField = props.baseField;
    const customOptions: TOptionValue[] = props.customOptions ?? [];
    const {t} = config;
    const {isBooleanOptions = false, fieldErrors} = props;

    const optionValues = baseField.optionValues ?? [];

    const renderFieldErrors = () => {
        return (
            fieldErrors?.map((fieldError) => (
                <div className="invalid-feedback" key={fieldError}>
                    {fieldError}
                </div>
            ))
        );
    }

    const createOptions = (options: TOptionValue[]) => {
        // Do these options have a group ?
        if (options && options.length > 0 && StringUtils.getNString(options[0].optionGroup) !== "") {
            let groups: Record<string, TOptionValue[]> = {};

            options.forEach((opt) => {
                if (opt.optionGroup) {
                    groups = {...groups, [opt.optionGroup]: [...(groups[opt.optionGroup] ?? []), opt]};
                }
                //groups = {...groups, [opt.label]: [...groups[opt.label] ?? {}, opt]};
            });

            return Object.keys(groups).map((groupLabel) => (
                <optgroup label={groupLabel} key={groupLabel}>
                    {groups[groupLabel].map(option => (
                        <option className={option.classNameAttribute ?? undefined} key={option.value} value={option.value} disabled={option.disabled}>
                            {translate(option.label, baseField, t)}
                        </option>
                    ))}
                </optgroup>
            ));
        }
        else {
            return options.map((option) => (
                    <option className={option.classNameAttribute ?? undefined} key={option.value} value={option.value} disabled={option.disabled}>
                        {translate(option.label, baseField, t)}
                    </option>
                )
            );
        }
    }

    return (
        <>
            <select id={props.id ?? baseField.id}
                    name={props.name ?? props.id ?? baseField.id}
                    className={"select " + (optionValues.find(option => option.value === field.value)?.classNameAttribute ?? "") + " form-control " + ((fieldErrors && fieldErrors?.length > 0) ? "is-invalid" : "")}
                    placeholder={props.placeholder ?? t(baseField.label ?? "")}
                    value={props.value ?? (_ => {
                        if (isBooleanOptions) {
                            return field.value == null ? "" : field.value.toString();
                        }

                        return field.stringValue;
                    })()
                    }
                    onChange={jsEvent => {
                        if (props.onChange) {
                            let fieldValue: TFieldValue | null = jsEvent.target.value;
                            if (isBooleanOptions) {
                                fieldValue = fieldValue === "" ? null : TypeUtils.getBooleanValue(fieldValue);
                            }
                            props.onChange(field, fieldValue);
                            if (props.tWebForm
                                && props.setTWebForm
                                && baseField.customObjectParams?.ON_CHANGE_ACTION
                                && StringUtils.getNString(baseField.customObjectParams?.ON_CHANGE_ACTION) !== "") {
                                    config.fieldSelectHandlers[baseField.customObjectParams.ON_CHANGE_ACTION](
                                        field,
                                        props.tWebForm,
                                        props.setTWebForm,
                                        props.onChange
                                    );
                            }
                        }
                    }}
            >
                <option value={""} className="text-secondary" disabled={baseField.isMandatory}>
                    {props.blankOptionLabel ? t(props.blankOptionLabel) : "..."}
                </option>
                {baseField.optionValues && (
                  createOptions(baseField.optionValues)
                )}
                {customOptions?.map(option => {
                    return (
                        <option className={option.classNameAttribute ?? undefined} key={option.value} value={option.value}>
                            {t(option.label)}
                        </option>
                    );
                })}
            </select>
            {renderFieldErrors()}
        </>
    );
}

export default FieldSelect;
