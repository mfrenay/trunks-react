import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";

import TField, {TFieldAction, TFieldState} from "../../../../trunks-core-js/model/TField";
import TRow from "../../../../trunks-core-js/model/TRow";
import Logger from "../../../../trunks-core-js/logger/logger";
import TList from "../../../../trunks-core-js/model/TList";
import StringUtils from "../../../../trunks-core-js/utilities/StringUtils";
import {CallbackFunction} from "../../list/field/ListField";
import Modal from "../../modal/Modal";
import {ModalAdvancedSearch} from "../../search/AdvancedSearch";
import {config} from "../../../config";

interface IProps {
    id: string,
    field: TField,
    baseField: TField,
    tRow: TRow,
    tList : TList,

    additionalCallbacks?: Record<string, CallbackFunction>,
    onRowFieldAction?: ((tRow: TRow, tList: TList, action: string) => void)
}

/**
 * FieldImage
 * @param props
 * @constructor
 */
const FieldImage = (props: IProps) => {

    const history = useHistory();

    const [showModalAdvancedSearch, setShowModalAdvancedSearch] = useState(false);
    const [dataSourceId, setDataSourceId] = useState("");

    useEffect(() => {
    }, [showModalAdvancedSearch]);

    /**
     * modalAdvancedSearch
     *
     * Display the Generic ModalAdvancedSearch when the state is changed
     */
    const modalAdvancedSearch = () => {

         if (showModalAdvancedSearch) {
            return (
                <Modal id={"modalAdvancedSearchFromFieldImage" + dataSourceId} className="modal-advanced-search-from-field" >
                    {/*withModalUUID(AdvancedSearch) TODO: Find a way to give a uuid to a modal and close it using this uuid */}

                    <ModalAdvancedSearch
                        // AdvancedSearch props
                        dataSourceId={dataSourceId}
                        tRowParams={tRow}
                        searchAtFirstDisplay={true}
                        enableDelete={false}
                        onCancel={handleCancelModalAdvancedSearch}
                        title={baseField.customObjectParams?.POPUP_TITLE && config.t(baseField.customObjectParams?.POPUP_TITLE)}
                    />
                </Modal>
            );
        }

        return null;
    }

    /**
     * onCancelModalAdvancedSearch
     *
     * Callback : function to call if user clicks on "Cancel" button of the AS
     */
    const handleCancelModalAdvancedSearch = () => {
        setShowModalAdvancedSearch(false);
        setDataSourceId("");
    }

    /**
     * executeAction
     *
     * Callback : function called when user clicks on the button
     */
    const executeAction = (tRow: TRow, tList: TList, action: string) => {
        Logger.d(">>>> FieldImage.executeAction - action = " + action, tRow);

        let idWebForm = "";
        if (action.indexOf("=") !== -1) {
            const parts = action.split("=");
            action = parts[0];
            idWebForm = parts[1];
        }
        else if (props.onRowFieldAction != null) {
            Logger.d(">>>> FieldImage.executeAction - onRowFieldAction is not null ", props.onRowFieldAction);
            props.onRowFieldAction!(tRow, tList, action);
            return;
        }

        if (action === TFieldAction.OPEN_LIST) {
            if (idWebForm === "") {
                Logger.d("FieldImage.executeAction - '" + action + "' does not provide ID_LIST to open !");
            } else {
                const routePath = StringUtils.getEString(baseField.customObjectParams?.ROUTE_PATH, "/list");
                history.push(routePath, {idList: idWebForm, tRowSource: tRow});
            }
        }
        else if (action === TFieldAction.OPEN_LIST_POPUP) {
            if (idWebForm === "") {
                Logger.d("FieldImage.executeAction - '" + action + "' does not provide ID_LIST to open !");
            } else {
                Logger.d("FieldImage.executeAction - '" + action + "' - open modal");
                setShowModalAdvancedSearch(true);
                setDataSourceId(idWebForm);
            }
        }
        else if (action === TFieldAction.OPEN_FORM) {
            if (idWebForm === "") {
                Logger.d("FieldImage.executeAction - '" + action + "' does not provide ID_FORM to open !");
            } else {
                const routePath = StringUtils.getEString(baseField.customObjectParams?.ROUTE_PATH, "/form");
                history.push(routePath, {idList: idWebForm, tRowSource: tRow});
            }
        }
        else if (action === TFieldAction.EDIT_FORM) {
            Logger.d("FieldImage.executeAction - '" + action + "' ...");
            const additionalCallbacks = props.additionalCallbacks;
            if (additionalCallbacks != null) {
                const onEditForm: CallbackFunction = additionalCallbacks["onEditForm"];
                if (onEditForm != null) {
                    onEditForm(null, tList, tRow);
                }
            }
        }
        else {
            Logger.d("FieldImage.executeAction - '" + action + "' is not managed !");
        }
    }

    const field: TField = props.field;
    const baseField: TField = props.baseField!;
    const tRow: TRow = props.tRow;
    const tList: TList = props.tList;

    if (StringUtils.stringToBoolean(baseField.customObjectParams?.FONTAWESOME) && StringUtils.getNString(baseField.action) !== "") {
        // enabled or disabled ?
        const isDisabled = field.stringValue === TFieldState.DISABLED;
        return (
            <>
                <button className={"button button-transparent button-np button-auto" + (isDisabled ? " btn-disabled" : "")}
                        disabled={isDisabled}
                        onClick={() => executeAction(tRow, tList, baseField.action!)}
                >
                    <i className={StringUtils.getEString(baseField.customObjectParams?.URL_IMAGE, field.stringValue)} />
                </button>
                {modalAdvancedSearch()}
            </>
        );
    }
    else if (StringUtils.stringToBoolean(baseField.customObjectParams?.FONTAWESOME)) {
        return (
            <i className={StringUtils.getEString(baseField.customObjectParams?.URL_IMAGE, field.stringValue)} />
        );
    }
    else if (StringUtils.getNString(baseField.action) !== "") {
        return (
            <>
                <img id={props.id ?? field.id}
                     className="field_image"
                     src={StringUtils.getNString(process.env.PUBLIC_URL) + field.stringValue}
                     alt={field.stringValue}
                     onClick={() => executeAction(tRow, tList, baseField.action!)}
                />
                {modalAdvancedSearch()}
            </>
        );
    }
    else {
        return (
            <>
                <img id={props.id ?? field.id}
                     className="field_image"
                     src={StringUtils.getNString(process.env.PUBLIC_URL) + field.stringValue}
                     alt={field.stringValue}
                />
                {modalAdvancedSearch()}
            </>
        );
    }

}

export default FieldImage;
