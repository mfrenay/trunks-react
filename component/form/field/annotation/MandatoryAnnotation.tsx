import React from "react";

function MandatoryAnnotation() {
    return <span className="mandatory__indicator">*</span>;
}

export default MandatoryAnnotation;
