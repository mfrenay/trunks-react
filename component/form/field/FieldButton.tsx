import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";

import TField, {TFieldAction} from "../../../../trunks-core-js/model/TField";
import TRow from "../../../../trunks-core-js/model/TRow";
import Logger from "../../../../trunks-core-js/logger/logger";
import TList from "../../../../trunks-core-js/model/TList";
import StringUtils from "../../../../trunks-core-js/utilities/StringUtils";
import {config} from "../../../config";
import Modal from "../../modal/Modal";
import {ModalAdvancedSearch} from "../../search/AdvancedSearch";

interface IProps {
    id: string,
    field: TField,
    baseField: TField,
    tRow: TRow,
    tList : TList,

    onRowFieldAction?: ((tRow: TRow, tList: TList, action: string) => void)
}

const FieldButton = (props: IProps) => {

    const history = useHistory();

    /**
     * executeAction
     *
     * Callback : function called when user clicks on the button
     */
    const executeAction = (tRow: TRow, tList: TList, action: string) => {
        Logger.d(">>>> FieldButton.executeAction - action = " + action, tRow);

        let idWebForm = "";
        if (action.indexOf("=") !== -1) {
            const parts = action.split("=");
            action = parts[0];
            idWebForm = parts[1];
        }
        else if (props.onRowFieldAction != null) {
            Logger.d(">>>> FieldImage.executeAction - onRowFieldAction is not null ", props.onRowFieldAction);
            props.onRowFieldAction!(tRow, tList, action);
            return;
        }

        if (action === TFieldAction.OPEN_LIST) {
            if (idWebForm === "") {
                Logger.d("FieldButton.executeAction - '" + action + "' does not provide ID_LIST to open !");
            } else {
                const routePath = StringUtils.getEString(baseField.customObjectParams?.ROUTE_PATH, "/list");
                history.push(routePath, {idList: idWebForm, tRowSource: tRow});
            }
        }
        else if (action ===TFieldAction.OPEN_LIST_POPUP) {
            if (idWebForm === "") {
                Logger.d("FieldButton.executeAction - '" + action + "' does not provide ID_LIST to open !");
            } else {
                // FIXME Seb - open modal
            }
        }
        else {
            Logger.d("FieldButton.executeAction - '" + action + "' is not managed !");
        }
    }

    const {t} = config;
    const field: TField = props.field;
    const baseField: TField = props.baseField!;
    const tRow: TRow = props.tRow;
    const tList: TList = props.tList;

    const [showModalAdvancedSearch, setShowModalAdvancedSearch] = useState(false);
    const [dataSourceId, setDataSourceId] = useState("");

    useEffect(() => {
    }, [showModalAdvancedSearch]);

    /**
     * modalAdvancedSearch
     *
     * Display the Generic ModalAdvancedSearch when the state is changed
     */
    const modalAdvancedSearch = () => {

        if (showModalAdvancedSearch) {
            return (
                <Modal id={"modalAdvancedSearchFromFieldImage" + dataSourceId} className="modal-advanced-search-from-field" >
                    {/*withModalUUID(AdvancedSearch) TODO: Find a way to give a uuid to a modal and close it using this uuid */}

                    <ModalAdvancedSearch
                        // AdvancedSearch props
                        dataSourceId={dataSourceId}
                        tRowParams={tRow}
                        searchAtFirstDisplay={true}
                        enableDelete={false}
                        onCancel={handleCancelModalAdvancedSearch}
                        title={baseField.customObjectParams?.POPUP_TITLE && config.t(baseField.customObjectParams?.POPUP_TITLE)}
                    />
                </Modal>
            );
        }

        return null;
    }

    /**
     * onCancelModalAdvancedSearch
     *
     * Callback : function to call if user clicks on "Cancel" button of the AS
     */
    const handleCancelModalAdvancedSearch = () => {
        setShowModalAdvancedSearch(false);
        setDataSourceId("");
    }

    return (
        <>
            <button id={props.id ?? field.id} className="button button-secondary button-xs button-p-xs button-auto"
                    onClick={() => executeAction(tRow, tList, baseField.action!)}>
                {t(baseField.stringValue)}
            </button>
            {modalAdvancedSearch()}
        </>
    );
}

export default FieldButton;
