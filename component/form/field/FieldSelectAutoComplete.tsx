import React, {CSSProperties, useMemo} from "react";

import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import TOptionValue from "../../../../trunks-core-js/model/TOptionValue";
import TypeUtils from "../../../../trunks-core-js/utilities/TypeUtils";
import {config} from "../../../config";
import StringUtils from "trunks-core-js/utilities/StringUtils";
import TWebForm from "trunks-core-js/model/TWebForm";
import {translate} from "trunks-core-js/utilities/TranslationUtils";
import Select from "react-select";

interface IProps {
  id?: string;
  name?: string;
  placeholder?: string;
  value?: string | string[] | number;

  // Label displayed for the option with blank value ("")
  blankOptionLabel?: string;

  baseField: TField,
  field: TField,
  isBooleanOptions?: boolean,
  customOptions?: TOptionValue[],
  onChange?: (field: TField, value: TFieldValue | null) => void,

  fieldErrors?: string[],

  tWebForm?: TWebForm,
  setTWebForm?: (tWebForm: TWebForm) => void
}

interface Option {
  label: string,
  value: string
}

interface OptionGroup {
  label: string,
  options: Option[];
}

function createOptions(tOptionValues: TOptionValue[],
                       customOptions: TOptionValue[],
                       isBooleanOptions: boolean,
                       translateFn: (_: string) => string
): OptionGroup[] | Option[] {

  const createOptionFromTOptionValue = (option: TOptionValue) => ({
    label: translateFn(option.label),
    value: (isBooleanOptions ? (option.value == null ? "" : option.value.toString()) : option.value),
    isDisabled: option.disabled,
    className: option.classNameAttribute,
  });

  // Do these options have a group ?
  if (tOptionValues && tOptionValues.length > 0 && StringUtils.getNString(tOptionValues[0].optionGroup) !== "") {
    let groups: Record<string, TOptionValue[]> = {};

    tOptionValues.forEach((opt) => {
      if (opt.optionGroup) {
        groups = {...groups, [opt.optionGroup]: [...(groups[opt.optionGroup] ?? []), opt]};
      }
    });

    return [
      ...Object.keys(groups).map((groupLabel) => ({
        label: groupLabel,
        options: groups[groupLabel].map(createOptionFromTOptionValue)
      }))
    ];
  } else {
    return [
      ...tOptionValues.map(createOptionFromTOptionValue),
      ...customOptions.map(createOptionFromTOptionValue)
    ];
  }
}

function findValueInGroup(groups: { label: string, options: { label: string, value: string }[] }[], value: any) {
  for (const group of groups) {
    const ret = group.options.find(opt => opt.value === value);

    if (ret) return ret;
  }
}

const groupStyles: CSSProperties = {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
};

const groupBadgeStyles: CSSProperties = {
  backgroundColor: "var(--color-grey03)",
  borderRadius: "2em",
  color: "var(--color-grey09)",
  display: "inline-block",
  fontSize: 12,
  fontWeight: "normal",
  lineHeight: "1",
  minWidth: 1,
  padding: "0.16666666666667em 0.5em",
  textAlign: "center",
};

function FieldSelect(props: IProps) {
  const field: TField = props.field;
  const baseField: TField = props.baseField;
  const {t} = config;
  const {isBooleanOptions = false, fieldErrors} = props;

  const options = useMemo(() => {
    return createOptions(
      baseField.optionValues ?? [],
      props.customOptions ?? [],
      isBooleanOptions,
      (opt) => translate(opt, baseField, t)
    );
  }, [baseField, props.customOptions, isBooleanOptions, t]);

  const optionValues = baseField.optionValues ?? [];

  const renderFieldErrors = () => {
    return (
      fieldErrors?.map((fieldError) => (
        <div className="invalid-feedback" key={fieldError}>
          {fieldError}
        </div>
      ))
    );
  }

  const hasGroups = () => (
    !!baseField.optionValues?.[0]?.optionGroup
  );

  return (
    <>
      <Select<Option, false, OptionGroup>
        id={props.id ?? baseField.id}
        name={props.name ?? props.id ?? baseField.id}
        className={"select form-control "
          + (optionValues.find(option => option.value === field.value)?.classNameAttribute ?? "")
          + ((fieldErrors && fieldErrors?.length > 0) ? "is-invalid" : "")
        }
        placeholder={props.placeholder ?? t(baseField.label ?? "")}
        options={options}
        value={hasGroups()
          ? /* grouped options */ findValueInGroup((options as OptionGroup[]), props.value ?? field.value)
          : /* simple options */ (options as Option[]).find(opt => opt.value === (props.value ?? field.value))
        }
        isSearchable
        unstyled
        styles={{
          placeholder: base => ({
            ...base,
            color: "var(--color-grey06)"
          }),
          container: (base) => ({
            ...base,
            "paddingBlock": "0",
            "backgroundImage": "url()",
          }),
          indicatorsContainer: (base) => ({
            ...base,
            color: "var(--color-primary02)",
          }),
          menu: (base) => ({
            ...base,
            overflow: "hidden",
            //border: "1px var(--color-grey02) solid",
            boxShadow: "var(--color-grey03) 0 0 10px 0px",
            transform: "translateX(-.75rem)"
          }),
          groupHeading: base => ({
            ...base,
            backgroundColor: "var(--color-grey01)",
            color: "var(--color-grey05)",
            padding: "1rem .5rem .5rem .5rem",
            fontWeight: "bold"
          }),
          option: (base, state) => ({
            ...base,
            backgroundColor: state.isFocused ? "var(--color-primary01)" : "#fff",
            padding: ".5rem",
            "&:hover": {
              ...base,
              background: "var(--color-primary01)",
            }
          }),
        }}
        formatGroupLabel={(data) => (
          <div style={groupStyles}>
            <span>{data.label}</span>
            <span style={groupBadgeStyles}>{data.options.length}</span>
          </div>
        )}
        noOptionsMessage={() => t("trunks:select_auto_complete.no_option")}
        isClearable={!baseField.isMandatory}
        onChange={(selectedOption) => {
          if (props.onChange) {
            let fieldValue: TFieldValue | null = selectedOption?.value ?? null;
            if (isBooleanOptions) {
              fieldValue = fieldValue === "" ? null : TypeUtils.getBooleanValue(fieldValue);
            }
            props.onChange(field, fieldValue);
            if (props.tWebForm
              && props.setTWebForm
              && baseField.customObjectParams?.ON_CHANGE_ACTION
              && StringUtils.getNString(baseField.customObjectParams?.ON_CHANGE_ACTION) !== "") {
              config.fieldSelectHandlers[baseField.customObjectParams.ON_CHANGE_ACTION](
                field,
                props.tWebForm,
                props.setTWebForm,
                props.onChange
              );
            }
          }
        }}
      />
      {renderFieldErrors()}
    </>
  );
}

export default FieldSelect;
