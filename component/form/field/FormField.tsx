import React from "react";

import TField, {DisplayType} from "../../../../trunks-core-js/model/TField";
import Logger from "../../../../trunks-core-js/logger/logger";
import TFieldKind from "../../../../trunks-core-js/model/TFieldKind";
import StringUtils from "../../../../trunks-core-js/utilities/StringUtils";

import FieldInput from "./FieldInput";
import FieldSelect from "./FieldSelect";
import FieldCheckBox from "./FieldCheckBox";
import FieldIconPicker from "./FieldIconPicker";
import FieldLabel from "./FieldLabel";
import FieldColorPicker from "./FieldColorPicker";
import FieldTextArea from "./FieldTextArea";
import FieldMultiCheckBox from "./FieldMultiCheckBox";
import MandatoryAnnotation from "./annotation/MandatoryAnnotation";
import {config} from "../../../config";
import FieldToggle from "./FieldToggle";
import FieldSelectAutoComplete from "./FieldSelectAutoComplete";

// FIXME: Define IProps
function FormField(props: any) {
    const {t} = config;
    const {tForm, fieldId, className, ...otherPropsWithoutBaseField} = props;
    const baseField: TField = tForm.baseRow.getFieldById(fieldId)!;
    const field: TField = tForm.getFieldById(fieldId);

    // Add baseField and field to otherProps that will be passed to Fields component
    const otherProps = Object.assign({}, otherPropsWithoutBaseField, {baseField: baseField, field: field});

    const displayAnnotation = () => {
        if (baseField.isMandatory) {
            return <MandatoryAnnotation />
        }
        return null;
    }

    // Check if the field has a custom display
    switch (baseField.customDisplay) {
        case null: break;
        case DisplayType.COLOR_PICKER:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldColorPicker {...otherProps} />
                </div>
            );
        case DisplayType.ICON_PICKER:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldIconPicker {...otherProps} />
                </div>
            );
        case DisplayType.MULTI_CHECKBOX:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldMultiCheckBox {...otherProps} />
                </div>
            );
        case DisplayType.SELECT_AUTO_COMPLETE:
            return (
              <div className={"textfield " + StringUtils.getNString(props.className)}>
                <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                <FieldSelectAutoComplete
                  {...otherProps}
                  blankOptionLabel={baseField.customObjectParams?.BLANK_OPTION_LABEL}
                  fieldErrors={props.fieldErrors?.[fieldId]}
                />
              </div>
            );
        default:
            const msg = `Unknown custom display ${baseField.customDisplay} for field ${baseField.id}`;
            Logger.e(msg);
            throw new Error(msg);
    }

    let ruleForceLabel = false;
    if (baseField.forceLabelIf != null) {
        ruleForceLabel = field.isForceLabel(baseField, tForm.row, tForm);
    }

    // Force label field - a way to display a form in readonly
    if (props.forceLabel || ruleForceLabel) {
        return (
            <div className={"textfield " + StringUtils.getNString(props.className)}>
                <label htmlFor={baseField.id}>{t(baseField.label!)}</label><span className={"label-two-points"}>&nbsp;:&nbsp;</span>
                <FieldLabel {...otherProps} />
            </div>
        );
    }

    //Logger.d("<<<< FormField - props.fieldErrors?.["+fieldId+"]" + props.fieldErrors?.[fieldId]);

    // Return the right field according to the field's kind
    switch (baseField.kind) {
        case null:
        case TFieldKind.NONE:
        case TFieldKind.TEXTFIELD:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldInput {...otherProps} fieldErrors={props.fieldErrors?.[fieldId]}/>
                </div>
            );
        case TFieldKind.TEXTAREA:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldTextArea {...otherProps} />
                </div>
            );
        case TFieldKind.CHECKBOX:
            return (
                <>
                    <label className={"checkbox -alt " + StringUtils.getNString(props.className)} htmlFor={baseField.id}>
                        <FieldCheckBox {...otherProps} fieldErrors={props.fieldErrors?.[fieldId]}/>
                        <div className="checkbox__label">{t(baseField.label!)}</div>
                    </label>
                    {props.fieldErrors?.[fieldId]?.map((fieldError: string) => (
                        <div className="invalid-feedback" key={fieldError} style={{display: "block"}}>
                          {fieldError}
                        </div>
                    ))
                  }
                </>
            );
        case TFieldKind.TOGGLE:
            return (
                <>
                    <div className={"textfield " + StringUtils.getNString(props.className)}>
                        <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                        <FieldToggle {...otherProps} fieldErrors={props.fieldErrors?.[fieldId]}/>
                    </div>
                </>
            );
        case TFieldKind.LABEL:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}</label><span className={"label-two-points"}>&nbsp;:&nbsp;</span>
                    <FieldLabel {...otherProps} />
                </div>
            );
        case TFieldKind.LIST_VAL:
        case TFieldKind.SELECT:
            return (
                <div className={"textfield " + StringUtils.getNString(props.className)}>
                    <label htmlFor={baseField.id}>{t(baseField.label!)}{displayAnnotation()}</label>
                    <FieldSelect
                      {...otherProps}
                      blankOptionLabel={baseField.customObjectParams?.BLANK_OPTION_LABEL}
                      fieldErrors={props.fieldErrors?.[fieldId]}
                    />
                </div>
            );
        default:
            return null;
    }
}

export default FormField;
