import React, {useEffect, useRef} from "react";
import DatePicker, {registerLocale} from "react-datepicker";
import fr from "date-fns/locale/fr";

import TField, {TFieldValue} from "trunks-core-js/model/TField";
import DateUtils from "trunks-core-js/utilities/DateUtils";
import {config} from "../../../config";

import "react-datepicker/dist/react-datepicker.min.css";

registerLocale('fr', fr);

const TIME_FORMAT = "HH:mm";

interface IProps {
    id?: string;
    disabled?: boolean;
    name?: string;
    placeholder?: string;
    type?: string;
    value?: string | string[] | number;
    focusField?: string;

    baseField: TField;
    field: TField;
    onChange?: (field: TField, value: TFieldValue | null) => void;

    timeIntervals?: number;

    fieldErrors?: string[]
}

function FieldTimeInput(props: IProps) {
    const {t, i18n} = config;
    const {field, baseField, fieldErrors} = props;

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (props.focusField === baseField.id) {
            inputRef?.current?.focus();
        }
    }, [props.focusField, baseField]);

    const renderFieldErrors = () => {
        return (
            fieldErrors?.map((fieldError) => (
                <div className="invalid-feedback" style={{display: "block"}} key={fieldError}>
                    {fieldError}
                </div>
            ))
        );
    }

    return (
        <>
            <DatePicker
                locale={i18n.language}
                className={"form-control " + ((fieldErrors && fieldErrors?.length > 0) ? "is-invalid" : "")}
                selected={field.stringValue === "" ? null : new Date("2000-01-01 " + field.stringValue)}
                showTimeSelect
                showTimeSelectOnly
                dateFormat={TIME_FORMAT}
                timeIntervals={props.timeIntervals ?? 15}
                timeCaption={t("field_input.label_date_picker_time")}
                placeholderText={props.placeholder ?? t(baseField.label ?? "")}

                onChange={(date: Date) => {
                    props.onChange?.(field, date === null ? null : DateUtils.getFormattedHourHHMM(date));
                }}
            />
            {renderFieldErrors()}
        </>
    );
}

export default FieldTimeInput;
