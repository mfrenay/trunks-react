import React from "react";
import {ColorResult, SketchPicker} from "react-color";

import TField from "../../../../trunks-core-js/model/TField";

//import "./css/fieldColorPicker.scss"

class FieldColorPicker extends React.Component<any, any> {

    constructor(props: any) {
        super(props);

        this.state = {
            displayColorPicker: false,
            color: this.props.field.stringValue,
        }
    }

    componentDidMount() {
    }

    render() {
        const field: TField = this.props.field;
        return (
            <div className="color-picker-container">
                <div className="color-picker-display" onClick={ _ => this.setState({displayColorPicker: true})}>
                    <div className="color-picker" style={{backgroundColor: this.state.color}}/>
                </div>
                {this.state.displayColorPicker && !this.props.forceLabel && (
                    <div className="color-picker-popover">
                        <div className="cover" onClick={ _ => this.setState({displayColorPicker: false})}/>
                        <SketchPicker color={this.state.color}
                            onChange={(color: ColorResult, event: React.ChangeEvent<HTMLInputElement>) => {
                                this.setState({color: color.hex});
                                this.props.onChange(field, color.hex);
                            }}
                        />
                    </div>
                )}

            </div>
        );
    }
}

export default FieldColorPicker;
