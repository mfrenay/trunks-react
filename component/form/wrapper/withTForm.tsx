import React, {PropsWithChildren} from "react";

import ApiResponse from "trunks-core-js/web-service/dto/ApiResponse";
import TForm from "../../../../trunks-core-js/model/TForm";
import Logger from "../../../../trunks-core-js/logger/logger";
import TField, {TFieldValue} from "../../../../trunks-core-js/model/TField";
import StringUtils from "../../../../trunks-core-js/utilities/StringUtils";
import TSaveFormDataDTO from "../../../../trunks-core-js/web-service/dto/TSaveFormDataDTO";
import TrunksWebService from "../../../../trunks-core-js/web-service/TrunksWebService";
import TRow from "../../../../trunks-core-js/model/TRow";
import TRowState from "../../../../trunks-core-js/model/TRowState";
import GenericValidationUtils, {IFormError, IRowError} from "../../../utilities/GenericValidationUtils";

import {ACTION_DUPLICATE, ListActionType} from "../../modal/wrapper/ITFormProps";
import ErrorManager from "../../../ui/ErrorManager";
import {config} from "../../../config";
import WebServiceManager from "../../../manager/WebServiceManager";


export interface IPropsTForm {
    tForm: TForm | null,
    formErrors: IFormError,
    isSubmitting: boolean,
    setFormErrors: (errors: IFormError | string[]) => void,
    onChangeField: (field: TField, value: TFieldValue | null, callback?: (tForm: TForm | null) => void) => void,
    onSave: (handleSaveFormArgs?: IHandleSaveFormArgs) => void,
    onCancel?: (...optionalArgs: any[]) => void,
    setStateTForm: (tForm: TForm) => void,
}

/**
 * Defines argument types of the withTForm().handleSaveForm method
 */
export interface IHandleSaveFormArgs {
    validationCallback?: () => IFormError | string[],
    customSave?: (tForm: TForm, ...optionalArgs: any[]) => void,
    optionalArgs?: any[],
    onSaveSuccess?: (tForm: TForm, jsonResponse: ApiResponse) => void,
    onSaveError?: (jsonResponse: ApiResponse) => void
}

function withTForm(WrappedComponent: any) {

    interface IProps extends PropsWithChildren<any> {
        dataSourceId?: string,   // If not specified, onSave must be given and preventDefaultSave must be true
        tForm?: TForm,
        newFormFallback?: boolean,
        tRowParams?: TRow,          // FIXME: convert TRow to TField[]
        defaultValues?: TField[],
        primaryKeys?: TField[], // if primaryKeys == undefined, this is a new Form
        listAction?: ListActionType,
        onSave?: (tForm: TForm, ...optionalArgs: any[]) => void,
        onCancel?: (...optionalArgs: any[]) => void,
        onError?: (error : any) => void,
        preventDefaultOnSave?: boolean // false if it needs to call default onSave AND onSave callback, true to call only onSave callback
        afterLoad?: (...params: any) => any,
    }

    interface IState {
        tForm: TForm | null,
        formErrors: IFormError,
        isSubmitting: boolean,
    }

    class WithTForm extends React.Component<IProps, IState> {

        constructor(props: IProps) {
            super(props);

            this.state = {
                tForm: props.tForm ?? null,
                formErrors: {
                    generalErrors: [],
                    fieldErrors: {}
                },
                isSubmitting: false,
            }
        }

        /**
         * componentDidMount
         *
         * Existing form : Load TForm data from server
         * New form : Call server to get structure of the TForm to manage the form data
         */
        componentDidMount() {
            if (this.state.tForm == null && this.props.dataSourceId && StringUtils.getNString(this.props.dataSourceId) !== "") {
                // Retrieve primary keys from props and make a string array with them
                const {primaryKeys} = this.props;

                const dataWebService = new TrunksWebService();

                const isNew = primaryKeys == null || primaryKeys.length === 0;

                if (!isNew) {
                    // EXISTING FORM (with data values)
                    dataWebService.getFormData((tForm: TForm) => {
                        Logger.d("<<<<<<< withTForm.componentDidMount - listAction = " + this.props.listAction);

                        if (tForm.rowState == null) {
                            // Set the state of the existing TForm
                            tForm.rowState = TRowState.UNCHANGED;
                        }

                        // if action = DUPLICATE, clear primary key values
                        if (this.props.listAction === ACTION_DUPLICATE) {
                            tForm.row.getPrimaryKeyField(tForm.baseRow).value = "";

                            // Set the state of the new TForm
                            tForm.rowState = TRowState.NEW;
                        }
                        this.setState({tForm: tForm});

                        // Callback after the first request has succeed
                        this.props.afterLoad?.();
                    }
                    , (error) =>{
                        Logger.e("<<<<<<< withTForm.componentDidMount.errorCallback...");

                        const {t} = config;
                        this.setState({formErrors: Object.assign({}, this.state.formErrors, {generalErrors: [...(this.state.formErrors.generalErrors ?? []), t("default.msg_error." + error.message)]})});

                        ErrorManager.displayErrorMessage(error);

                        this.props.onError?.(error);
                    }
                    , this.props.dataSourceId, primaryKeys!, this.props.tRowParams, this.props.newFormFallback)
                    .catch(error => {
                        Logger.e("<<<<<<< withTForm.componentDidMount.error...", error);
                        this.setState(() => { throw error; });
                    });
                } else {
                    // NEW FORM (with empty fields)
                    dataWebService.getNewFormData((tForm: TForm) => {
                        // Set the state of the new TForm
                        tForm.rowState = TRowState.NEW;

                        // set default values
                        this.props.defaultValues?.forEach(defaultField => {
                            const formField = tForm.getFieldById(defaultField.id);
                            if( formField !== null) {
                                formField.value = defaultField.value;
                            }
                        });

                        this.setState({tForm: tForm});

                        // Callback after the first request has succeed
                        this.props.afterLoad?.();
                    }
                    , (error) =>{
                        Logger.e("<<<<<<< withTForm.componentDidMount.errorCallback...");

                        const {t} = config;
                        this.setState({formErrors: Object.assign({}, this.state.formErrors, {generalErrors: [...(this.state.formErrors.generalErrors ?? []), t("default.msg_error." + error.message)]})});

                        ErrorManager.displayErrorMessage(error);

                        this.props.onError?.(error);
                    }
                    , this.props.dataSourceId, this.props.tRowParams)
                    .catch(error => {
                        Logger.e("<<<<<<< withTForm.componentDidMount.error...", error);
                        this.setState(() => { throw error; });
                    });
                }
            }
        }

        /**
         *
         * @param tForm
         */
        setStateTForm = (tForm: TForm) => {
            this.setState({tForm: tForm});
        }

        /**
         * handleChangeField
         *
         * Called when the value of a field belonging to TForm has changed
         *
         * @param field
         * @param value
         * @param callback
         */
        handleChangeField = (field: TField, value: TFieldValue, callback?: (tForm: TForm | null) => void) => {
            let tForm = Object.assign(new TForm(""), {...this.state.tForm});

            tForm?.setFieldValueById(field.id, value);

            this.setState({ tForm: tForm });

            callback?.(tForm);
        }

        /**
         * handleFormErrors
         *
         * Called to display form errors
         */
        handleFormErrors = (errors: IFormError | string[]) => {
            if (errors instanceof Array) {
                this.setState({formErrors: {generalErrors: [...errors]}});
            } else {
                this.setState({formErrors: errors});
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Actions - Cancel
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        handleCancel = (event: any, ...optionalArgs: any[]) => {
            event?.preventDefault();
            this.props.onCancel?.(...optionalArgs);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Actions - Save
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * handleSaveForm
         *
         * 1. validate data (generic validation + custom validation if provided)
         * 2. call Web Service to save the tForm
         * 3. invoke callback onSave from parent
         */
        handleSaveForm = async ({validationCallback, customSave, optionalArgs = [], onSaveSuccess, onSaveError}: IHandleSaveFormArgs = {}) => {
            Logger.d(`withTForm(${getDisplayName(WrappedComponent)}).handleSaveForm...`);

            let errors = validation(this.state.tForm!, validationCallback);

            this.setState({formErrors: errors});

            // If there is at least one error, don't save form
            if ((errors.generalErrors && errors.generalErrors.length > 0) || (errors.fieldErrors && Object.keys(errors.fieldErrors).length > 0)) {
                return;
            }

            if (this.props.preventDefaultOnSave || customSave) {
                await customSave?.(this.state.tForm!, ...optionalArgs);
                await this.props.onSave?.(this.state.tForm!, ...optionalArgs);
            }
            else {
                let initialValues: TField[] | undefined;
                if (this.state.tForm) {
                    initialValues = this.state.tForm.baseRow.getPrimaryKeyFields(this.state.tForm.baseRow)
                      .map((field) => new TField(field.id, field.initialValue));
                }

                const tSaveFormDataDTO = new TSaveFormDataDTO({
                    row: this.state.tForm!.row,
                    initialValues: initialValues,
                    rowState: this.state.tForm!.rowState,
                    params: this.props.tRowParams,
                });

                this.setState({isSubmitting: true});

                const dataWebService = new TrunksWebService();
                await dataWebService.saveFormData(WebServiceManager.manageMessage(async (jsonResponse) => {

                    const primaryKeys: { [key: string]: string } = jsonResponse.data.primaryKeys;

                    Logger.d(`withTForm(${getDisplayName(WrappedComponent)}).handleSaveForm.saveFormData... `, primaryKeys, ...optionalArgs);

                    this.setState({isSubmitting: false});

                    this.setState({
                        tForm: Object.assign(new TForm(this.state.tForm!.id),
                          this.state.tForm,
                          {rowState: TRowState.UNCHANGED})
                    });

                    // Set the primary key value received from the server
                    Object.keys(primaryKeys).forEach(primaryKeyFieldId => {
                        const primaryField = this.state.tForm!.row.getFieldById(primaryKeyFieldId);
                        if (primaryField != null) {
                            primaryField.value = primaryKeys[primaryKeyFieldId];
                        }
                    });

                    // Call onSaveSuccess given by the caller of this function
                    // Can be useful to save another webForm just after saving this form
                    await onSaveSuccess?.(this.state.tForm!, jsonResponse);

                    // Call onSave defined as props on the component
                    await this.props.onSave?.(this.state.tForm!, ...optionalArgs);
                }), (jsonResponse) => {
                    Logger.d(`withTForm(${getDisplayName(WrappedComponent)}).handleSaveForm.saveFormData... `, jsonResponse);

                    this.setState({isSubmitting: false});
                    ErrorManager.displayErrorMessage(jsonResponse);

                    // Call onSaveError given by the caller of this function
                    onSaveError?.(jsonResponse);

                }, this.props.dataSourceId!, tSaveFormDataDTO)
                .catch(error => {
                    Logger.e("<<<<<<< withTForm.handleSaveForm.error...", error);
                    this.setState(() => { throw error; });
                });
            }
        }

        render() {
            // Isolate props to not override them when passing this.props to WrappedComponent
            const {
                tForm,
                formErrors,
                onChangeField,
                onSave,
                onCancel,
                setStateTForm,
                ...otherProps
            } = this.props;

            return (
                <WrappedComponent
                    tForm={this.state.tForm}
                    formErrors={this.state.formErrors}
                    setFormErrors={this.handleFormErrors}
                    onChangeField={this.handleChangeField}
                    onSave={this.handleSaveForm}
                    onCancel={this.handleCancel}
                    setStateTForm={this.setStateTForm}
                    isSubmitting={this.state.isSubmitting}
                    {...otherProps}
                />
            );
        }
    }

    // @ts-ignore
    WithTForm.displayName = `${WithTForm.name}(${getDisplayName(WrappedComponent)})`;

    return WithTForm;
}

/**
 *
 * @param tForm The TForm to validate. The generic validation is applied on this TForm.
 * @param validationCallback A custom validation function
 */
function validation(tForm: TForm, validationCallback?: () => IFormError | string[]): IFormError {
    let errors: IFormError = {
        generalErrors: [],
        fieldErrors: {}
    };

    // Generic validation (based on TForm)
    if (tForm) {
        errors = GenericValidationUtils.validateForm(tForm);
    }
    // Custom validation
    if (validationCallback) {
        const customErrors = validationCallback();

        Logger.i(">>>>>>> TEST :", customErrors);

        if (customErrors instanceof Array) {
            errors = {
                generalErrors: [...errors.generalErrors ?? [], ...Object.values(errors.fieldErrors ?? {}).flat(), ...customErrors]
            }
        } else {
            // Merge the generic errors with the Specific errors
            let fieldErrors: IRowError = {};
            Object.keys(errors.fieldErrors ?? {}).forEach((fieldId) => fieldErrors = {
                ...fieldErrors,
                [fieldId]: errors.fieldErrors![fieldId]
            });
            Object.keys(customErrors.fieldErrors ?? {}).forEach((fieldId) => fieldErrors = {
                ...fieldErrors,
                [fieldId]: [
                    ...(fieldErrors[fieldId] || []),
                    ...customErrors.fieldErrors![fieldId]
                ]
            });

            errors = {
                generalErrors: [...(errors.generalErrors ?? []), ...(customErrors.generalErrors ?? [])],
                fieldErrors: fieldErrors
            };
        }
    }

    return errors;
}

// FIXME: Put in another file
function getDisplayName(WrappedComponent: any) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withTForm;
