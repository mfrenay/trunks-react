import React from "react";

import TField, {DisplayType, TFieldValue} from "../../../../trunks-core-js/model/TField";
import TRow from "../../../../trunks-core-js/model/TRow";
import TList from "../../../../trunks-core-js/model/TList";
import TFieldKind from "../../../../trunks-core-js/model/TFieldKind";

import FieldLabel from "../../form/field/FieldLabel";
import FieldCheckBox from "../../form/field/FieldCheckBox";
import FieldButton from "../../form/field/FieldButton";
import FieldImage from "../../form/field/FieldImage";
import {config} from "../../../config";
import FieldInput from "../../form/field/FieldInput";
import FieldSelect from "../../form/field/FieldSelect";
import FieldTimeInput from "../../form/field/FieldTimeInput";

export type CallbackFunction = ((...data: any) => any) | undefined;

interface IProps {
    baseField: TField;
    row: TRow;
    tList?: TList;
    onRefresh?: (tList : TList) => void;
    onChange?: (field: TField, value: TFieldValue | null) => void;
    onRowFieldAction?: (tRow: TRow, tList: TList, action: string) => void;

    // list of callbacks that could be called by Custom ListField.
    // Example :
    // A field that implements duplicate action could use a callback from the parent (e.g.: DataTable)
    additionalCallbacks?: Record<string, CallbackFunction>;
    refetch?: () => void;
    refetchSubList?: () => void;
}

export interface CustomDisplayListFieldProps {
    field: TField;
    tRow: TRow;
    tList?: TList;
    t: (key: (string | string[]), options?: any) => string;
    onRefresh?: (tList: TList) => void;
    onChange?: (field: TField, value: (TFieldValue | null)) => void;
    onRowFieldAction?: (tRow: TRow, tList: TList, action: string) => void;
    additionalCallbacks?: Record<string, CallbackFunction>;
    refetch?: () => void;
    refetchSubList?: () => void;
}

function ListField(props: IProps) {
    const {t} = config;
    const {row, tList, additionalCallbacks, onRowFieldAction, onRefresh, refetch, refetchSubList, ...otherProps} = props;
    const baseField: TField = props.baseField;
    const field = row.getFieldById(baseField.id)!;

    const fieldId = field.id + "." + row.nr;

    if (baseField.customDisplay != null /* && field.customDisplay typeof React.ComponentC*/) {
        switch (baseField.customDisplay) {
            // FIXME: Normally, this must be a normal field kind (DisplayType must be reserved for fully custom component)
            case DisplayType.TIME:
                return (
                    <FieldTimeInput id={fieldId} field={field} {...otherProps} />
                );

            default:
                const TagName = baseField.customDisplay;
                const customProps = {
                    field,
                    tRow: row,
                    tList,
                    additionalCallbacks,
                    t,
                    onRefresh,
                    refetch,
                    refetchSubList,
                    onRowFieldAction,
                    onChange: props.onChange
                };
                return React.createElement<CustomDisplayListFieldProps>(TagName, customProps);
        }

    }

    // Return the right field according to the field's kind
    switch (baseField.kind) {
        case null:
        case TFieldKind.NONE:
        case TFieldKind.LABEL:
            return (
                <FieldLabel id={fieldId} field={field} {...otherProps} />
            );
        case TFieldKind.LIST_VAL:
        case TFieldKind.SELECT:
            return (
                <FieldSelect id={fieldId}
                             field={field}
                             {...otherProps} />
            );
        case TFieldKind.TEXTFIELD:
            return (
                <FieldInput id={fieldId} field={field} {...otherProps} />
            );
        case TFieldKind.CHECKBOX:
            return (
                <FieldCheckBox id={fieldId} field={field} {...otherProps} />
            );
        case TFieldKind.BUTTON:
            return (
                <FieldButton id={fieldId} field={field} tList={tList!} tRow={row}
                             onRowFieldAction={onRowFieldAction}
                             {...otherProps} />
            );
        case TFieldKind.IMAGE:
            return (
                <FieldImage id={fieldId} field={field} tList={tList!} tRow={row}
                            additionalCallbacks={additionalCallbacks}
                            onRowFieldAction={onRowFieldAction}
                            {...otherProps}/>
            );
        default:
            return null;
    }
}

export default ListField;
