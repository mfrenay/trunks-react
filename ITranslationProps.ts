interface ITranslationProps {
    t: (key: string | string[], options?: any) => string,
    i18n: {
        language: string
    }
}

export function useTranslation(_: string) {
    return {
        t: (key: string) => ("*" + key + "*")
    }
}


export default ITranslationProps;
