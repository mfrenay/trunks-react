const translations = {
    trunks: {
        "default": {
            "btn_yes": "Oui",
            "btn_no": "Non",
            "btn_new": "Nouveau",
            "btn_cancel": "Annuler",
            "btn_delete": "Supprimer",
            "btn_save": "Sauvegarder",

            "msg_success": {
                "save_success": "Les données ont été correctement sauvegardées"
            },

            "msg_warning": {
                "save_data_not_changed": "Aucune modification n'a été détectée"
            },

            "msg_error": {
                "general": "Une erreur générale est survenue. Veuillez contacter l'administrateur.",
                "unauthorized": "Votre session a expiré ou vous n'êtes pas correctement authentifié.\nReconnectez-vous.",
                "bad_request": "Les données de la requête ne sont pas correctes",
                "forbidden": "Vous n'êtes pas autorisé à accéder à cette resource",
                "fk_constraint": "Cet élément ne peut pas être supprimé car il est référencé",

                "foreign_key_constraint_fails": {
                    "default": "Cet élément ne peut pas être supprimé car il est référencé"
                },

                "project": {
                },
            },

            "form_error": {
                "field_mandatory": "Le champ {{fieldLabel}} est obligatoire"
            },
        },

        "advanced_search": {
            "label_filters": "Recherche",
            "btn_apply": "Appliquer",
            "btn_export_xls": "Export XLS",
            "export_xls_filtered": "Liste filtrée",
            "export_xls_full": "Liste complète",
            "btn_exporting_xls": "Export XLS en cours...",

            "export_excel_failed": "Une erreur s'est produite lors de l'exportation du fichier Excel",

            "msg_save_success": "Sauvegarde effectuée avec succès",
        },

        "excel_import_file": {
            "import": "Importer",
            "import_success": "Le fichier a été importé avec succès",

            "error": {
                "import_failed": "Une erreur s'est produite lors de l'importation du fichier Excel",
                "file_required": "Veuillez sélectionner un fichier"
            }
        },

        "data_table": {
            "label_no_result": "Aucun résultat",
            "label_show": "Afficher",
            "label_entries": "lignes",
            "label_nb_rows": "Lignes {{indexFirst}} à {{indexLast}} sur {{nbTotalRows}}",
            "label_max_row_reached": "{{p1}} sur {{p2}} éléments sont affichés",
            "label_max_row_reached_use_filter": "Utiliser les filtres pour affiner le résultat",
            "btn_previous": "Précédent",
            "btn_next": "Suivant",

            "msg_confirm_delete": "Êtes-vous certain de vouloir supprimer cet élément ?",
            "msg_delete_success": "L'élément a été supprimé avec succès",
            "msg_insert_success": "L'élément a été ajouté avec succès",


            "badge_new": "NEW",

            "sum_title": "Total des {{length}} lignes"
        },

        "field_icon_picker": {
            "label_select_icon": "Sélectionner",
            "label_search_icon": "Rechercher une icône"
        },

        "auto_complete_input": {
            "ac_input_placeholder": "Sélectionner un élément...",
            "ac_input_prompt_text": "Tapez pour rechercher...",
            "ac_input_search_text": "Recherche en cours...",
            "ac_input_no_result": "Aucun résultat"
        },

        "field_input": {
            "label_date_picker_time": "Heure"
        },

        "search_field": {
            "label_yes": "Oui",
            "label_no": "Non"
        },

        "list_errors": {
            "row_line": "Ligne {{nrRow}}"
        },

        "select_auto_complete": {
            "no_option": "Aucun résultat"
        }
    }
};

export default translations;