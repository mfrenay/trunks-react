const translations = {
    trunks: {
        "default": {
            "btn_yes": "Yes",
            "btn_no": "No",
            "btn_new": "New",
            "btn_cancel": "Cancel",
            "btn_delete": "Delete",
            "btn_save": "Save",

            "msg_success": {
                "save_success": "Data has been saved successfully"
            },

            "msg_warning": {
                "save_data_not_changed": "No modification detected"
            },

            "msg_error": {
                "general": "A general error has occurred. Please contact the administrator.",
                "unauthorized": "Your session has expired or you are not properly authenticated.\nPlease, reconnect.",
                "bad_request": "The request data is incorrect",
                "forbidden": "You are not allowed to access this resource",
                "fk_constraint": "This element can not be deleted because it is referenced",

                "foreign_key_constraint_fails": {
                    "default": "This element can not be deleted because it is referenced",
                },

                "project": {
                },
            },

            "form_error": {
                "field_mandatory": "The field {{fieldLabel}} is mandatory",
            },
        },

        "advanced_search": {
            "label_filters": "Filters",
            "btn_apply": "Apply",
            "btn_export_xls": "XLS export",
            "export_xls_filtered": "Filtered list",
            "export_xls_full": "Full list",
            "btn_exporting_xls": "Exporting XLS...",

            "export_excel_failed": "An error occurred during the export of the Excel file",

            "msg_save_success": "Save completed successfully",
        },

        "excel_import_file": {
            "import": "Import",
            "import_success": "The file has been imported successfully",

            "error": {
                "import_failed": "An error occurred during the import of the Excel file",
                "file_required": "Please select a file"
            }
        },

        "data_table": {
            "label_no_result": "No result",
            "label_show": "Show",
            "label_entries": "entries",
            "label_nb_rows": "Row {{indexFirst}} to {{indexLast}} of {{nbTotalRows}}",
            "label_max_row_reached": "{{p1}} of {{p2}} records are displayed",
            "label_max_row_reached_use_filter": "Use filters to refine the result",
            "btn_previous": "Previous",
            "btn_next": "Next",

            "msg_confirm_delete": "Do you really want to delete this item ?",
            "msg_delete_success": "The item has been deleted successfully",
            "msg_insert_success": "The item has been inserted successfully",

            "badge_new": "NEW",

            "sum_title": "Total des {{length}} lignes"
        },

        "field_icon_picker": {
            "label_select_icon": "Select",
            "label_search_icon": "Search for an icon",
        },

        "auto_complete_input": {
            "ac_input_placeholder": "Select an item...",
            "ac_input_prompt_text": "Type to search...",
            "ac_input_search_text": "Searching...",
            "ac_input_no_result": "No matches found",
        },

        "field_input": {
            "label_date_picker_time": "Time",
        },

        "search_field": {
            "label_yes": "Yes",
            "label_no": "No",
        },

        "list_errors": {
            "row_line": "Row {{nrRow}}"
        },

        "select_auto_complete": {
            "no_option": "No result"
        }
    }
};

export default translations;