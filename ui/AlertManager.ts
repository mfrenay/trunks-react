// @ts-ignore
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer, ToastContent} from "react-toastify";

class AlertManager {
    static showErrorToast(msg: ToastContent) {
        toast.error(msg, {
            position: toast.POSITION.BOTTOM_RIGHT
        });
    }

    static showWarningToast(msg: ToastContent) {
        toast.warning(msg, {
            position: toast.POSITION.BOTTOM_RIGHT
        });
    }

    static showSuccessToast(msg: ToastContent) {
        toast.success(msg, {
            position: toast.POSITION.BOTTOM_RIGHT
        });
    }
}

export {ToastContainer}

export default AlertManager;
