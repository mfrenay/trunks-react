import AlertManager from "./AlertManager";
import Logger from "../../trunks-core-js/logger/logger";
import {config} from "../config";
import TranslationUtils from "../utilities/TranslationUtils";

export const ERROR_CODE = Object.freeze({
    unauthorized: 401,
});

class ErrorManager {

    /**
     * ErrorManager.displayWarningMessage
     * @param translationKey
     */
    static displayWarningMessage(translationKey: string)  {
        const {t} = config;

        Logger.w("******* ErrorManager.displayWarningMessage : ", translationKey);

        const fullTranslationKey = "default.msg_warning." + translationKey;
        const message = t(fullTranslationKey);
        if (message !== fullTranslationKey) {
            AlertManager.showWarningToast(message);
        }
    }

    /**
     * ErrorManager.displayErrorMessage(error, this.props.t);
     * @param error
     */
    static displayErrorMessage(error: any)  {
        const {t} = config;

        Logger.w("******* ErrorManager.displayErrorMessage : ", error);
        if (error.statusCode === ERROR_CODE.unauthorized || error.code === ERROR_CODE.unauthorized) {
            AlertManager.showErrorToast(t("default.msg_error.unauthorized"));
        }
        else {
            // First attempt to display specific message,
            const errorMessage = TranslationUtils.getErrorTranslation(t, error.message);
            if (errorMessage !== error.message) {
                AlertManager.showErrorToast(errorMessage);
            }
            else {
                // if not found, fallback to general error
                AlertManager.showErrorToast(t([
                    "default.msg_error." + error.message,
                    "default.msg_error.general"
                ]));
            }
        }
        return error.code;
    }
}

export default ErrorManager;
