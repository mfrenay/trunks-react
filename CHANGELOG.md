# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2023-06-05
### Added
- Manage dropdowns in add footer
- AdvancedSearch : refresh list after add row
- Management of a new custom param INSERT_ROW_ON_ADD 
  - Call api to insert the add row rather than insert in 'memory'
- Generic validation to check min and max values (based on MIN and MAX field custom params)
- Manage special option attribute OPTION_DISABLED
- i18n locale files with translations
- Display title (browser tooltip) on sum <td> cell in DataTable
- Add href props to FieldLabel
- Add color and size props to Loader component
- AdvancedSearch: change label and display loader in XLS export button
- New DropDown component
- New OutsideAlerter component that calls a handler when a click outside the subtree occurs
- New generic component FieldTimeInput
- New class TranslationUtils
- Manage saveList action in AdvancedSearch
- DataTable: Add refetch props to ListField (this will be passed to customDisplay component in order to initiate a refetch of the list)
- DataTable: Add refetchSublist props to ListField (this will be passed to customDisplay component in order to initiate a refetch of the sublist)
- New FieldSelectAutoComplete component for fields with 'SELECT_AUTO_COMPLETE' DisplayType

### Changed
- Optimization : management of the new custom param ONLY_SEND_ROWS_ON_FILTER
  - API filterList sends complete tList or just the rows of the tList 
- Best management of translation of a list error message sent by the back-end
- AdvancedSearch: export XLS is not a dropdown button proposing to export filtered list of full list
- Clone tForm on field change in withTForm HOC

### Fixed
- Type error in Modal component: createPortal need to be inside a React.Fragment
- AdvancedSearch: Reset the maxRowToRetrieve related properties on filtered list with ONLY_SEND_ROWS_ON_FILTER custom params
- Go back to first page when the tList has been filtered (in DataTable and AdvancedSearch)
- No more go back to first page when tList reference changed (unwanted behavior at some times when tList reference has changed, e.g. when only changing the value of a tRow)
  - NOTE: This implies that library users must manage the return to the first page themselves when necessary
- withTForm: Reset rowState on form save success.
- AdvancedSearch: search component disappears on search with DB sort
- ListErrors: add missing key props
- Fix FieldSelect props overridden in FormField (wrong order)
