import ITranslationProps from "./ITranslationProps";
import TField, {TFieldValue} from "trunks-core-js/model/TField";
import TWebForm from "trunks-core-js/model/TWebForm";

export type FieldSelectHandler = (tField: TField,
                           WebForm: TWebForm,
                           setTWebForm: (tWebForm: TWebForm) => void,
                           onChange?: (field: TField, value: TFieldValue | null) => void
) => void;

interface ConfigType extends ITranslationProps {
    fieldSelectHandlers: Record<string, FieldSelectHandler>
}

export let config: ConfigType = {
    t: (key) => ("*" + key + "*"),
    i18n: {
        language: "fr"
    },
    fieldSelectHandlers: {}
}

export function trunksReactConfigInit(newConfig: Partial<ConfigType>) {
    config = {
        ...config,
        ...newConfig
    };
}
